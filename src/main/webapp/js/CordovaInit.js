'use strict';

var CordovaInit = function () {
    var onDeviceReady = function () {
        receivedEvent('deviceready');
        var db = window.openDatabase("DeposcoPOS", "1.0", "Deposco POS DB", 1000000);
        db.transaction(initializeDB, errorCB, successCB);
        StatusBar.overlaysWebView(false);
        StatusBar.backgroundColorByHexString("#253A48");
    };

    var receivedEvent = function () {
        console.log('Start event received, bootstrapping application setup.');
        angular.bootstrap(document, ['myPos']);
    };

    function initializeDB(tx) {
        tx.executeSql('CREATE TABLE IF NOT EXISTS PREFS (key, value)');
    };

    function errorCB(err) {
        console.log("Error processing SQL: " + err.code);
    };

    function successCB() {
        console.log("success!");
    };

    this.bindEvents = function () {
        document.addEventListener('deviceready', onDeviceReady, false);
    };

    //If cordova is present, wait for it to initialize, otherwise just try to bootstrap the application.
    if (window.cordova !== undefined && navigator.appVersion.indexOf("iPad") != -1) {
        console.log('Cordova found, wating for device.');

        this.bindEvents();
    }
    else {
        console.log('Cordova not found, booting application');
        receivedEvent('manual');
    }
};

//$(function() {
console.log('Bootstrapping!');
new CordovaInit();
//});