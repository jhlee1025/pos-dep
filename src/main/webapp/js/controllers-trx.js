'use strict';

var posControllers = angular.module('posTrxControllers', []);

posControllers.controller('TransactionCtrl', function ($rootScope, $scope, $route, $http, $window, $location, $timeout, ngDialog) {

/*
    // logo url
    var logoUrl = 'http://images.deposco.com/'+$rootScope.user.company+'/pos_logo.gif';
    document.getElementById("posLogo").style.backgroundImage = "url('"+logoUrl+"')";

*/
    $scope.allowLineTotalDiscount = $rootScope.storeProfile.allowLineTotalDiscount;

    $scope.resetTrx = function () {
        $scope.paymentDue = 0;
        $scope.payAmount = "";
        $scope.preventEdit = false;
        $scope.paidAmount = "";

        $rootScope.currentCheckout = {
            "items": [],
            "customer": {},
            "payments": [],
            "discountRate": 0
        };

        $scope.subtotal = "";
        $scope.tax = "";
        $scope.cityTax = "";
        $scope.total = "";
        $scope.phoneNumber = "";
        $scope.notes = "";
        $scope.itemNumber = "";
        $scope.itemNumber2 = "";
        $scope.currentImgUrl = null;
        $scope.salesId = "";
        $scope.customerId = -1;
        $scope.storeCredit = 0;
        $scope.carrier = "";
        $scope.shipMethod = "";
        $scope.shippingCost = 0;
        $scope.captureShippingCost = false;
        $scope.currentImgUrl = "";
        $scope.warningMsg = "";
        $scope.customerEmail = "";
    };

    $scope.startCheckoutProcess = function () {

        $timeout(function(){ // to avoid timing issue

            if($rootScope.needManagerApproval) {
                var dcManagerOverride = ngDialog.open({template: 'templates/dc-manager-override.html', scope: $scope, controller: 'DCManagerOverrideCtrl'});

                dcManagerOverride.closePromise.then(function (data) {
                    var returnValue = data.value;
                    if (returnValue == 1) {
                        $rootScope.needManagerApproval = false;
                        checkOut();
                    }
                });
            }
            else {
                checkOut();
            }
        }, 100);
    };

    function checkOut() {
        if ($rootScope.currentCheckout.customer && $rootScope.currentCheckout.customer.id > 0) {
            $scope.customerId = $rootScope.currentCheckout.customer.id;
            $scope.storeCredit = $rootScope.currentCheckout.customer.storeCredit;
            $scope.customerEmail = $rootScope.currentCheckout.customer.email;
        }
        if ($rootScope.currentCheckout.salesId > 0) {
            $scope.salesId = $rootScope.currentCheckout.salesId;
        }

        for (var i = 0; i < $rootScope.currentCheckout.items.length; i++) {
            var item = $rootScope.currentCheckout.items[i];
            if (item.storeId != $rootScope.user.storeId) {
                if (!item.pickupOption || item.pickupOption == '') {
                    $scope.popupMsg = "Please select the fulfillment option for Item [" + item.name + "].";
                    ngDialog.open({scope: $scope});

                    return;
                }
            }
        }

        var checkoutDialog = ngDialog.open({template: 'templates/checkout.html', scope: $scope, controller: 'CheckoutCtrl', className: 'ngdialog-theme-mine'});
        checkoutDialog.closePromise.then(function (data) {
            var allowEdit = true;
            var returnValue = data.value;
            if (returnValue > 0) {
                $scope.resetTrx();

                if ($rootScope.storeProfile.promptPinNumber == true) {
                    var checkoutDialog = ngDialog.open({template: 'templates/switch-associate.html', scope: $scope, controller: 'SwitchAssociateCtrl'});

                    checkoutDialog.closePromise.then(function (data) {
                        var returnValue = data.value;

                        if (returnValue > 0) {
                            $scope.paymentDue = 0;
                            $scope.payAmount = "";
                            $scope.preventEdit = true;
                            $scope.paymentType = "";
                            $scope.showDebitPinInput = false;
                            $scope.transactData = {};
                            $scope.ccManualEntry = false;
                            $scope.salesId = "";

                            $rootScope.currentCheckout = {
                                "items": [],
                                "customer": {},
                                "payments": [],
                                "discountRate": 0
                            };

                            $scope.subtotal = 0;
                            $scope.tax = "";
                            $scope.cityTax = "";
                            $scope.total = "";
                            $scope.paymentProcessingStatus = "";

                            $location.$$search = {};
                            $location.path('/checkout');
                        }
                    })
                }
            }
        });
    }

    function calculateSubtotal() {
        var taxRate = $rootScope.storeProfile.taxRate;
        var cityTaxRate = $rootScope.storeProfile.cityTaxRate;

        if ($rootScope.currentCheckout && $rootScope.currentCheckout.items.length > 0) {
            for (var i = 0; i < $rootScope.currentCheckout.items.length; i++) {
                var item = $rootScope.currentCheckout.items[i];
                if (item.forceUntaxable == true) {
                    taxRate = 0.0;
                    cityTaxRate = 0.0;
                }
            }
        }

        if ($rootScope.currentCheckout && $rootScope.currentCheckout.items.length > 0) {
            var subtotal = 0;
            var totalDiscount = 0;
            var taxableAmount = 0;
            var subtotalBeforeDC = 0;
            $scope.tax = 0;
            $scope.cityTax = 0;

            for (var i = 0; i < $rootScope.currentCheckout.items.length; i++) {
                var item = $rootScope.currentCheckout.items[i];

                item.lineSubtotal = 0;
                item.lineSubtotalDue = 0;
                item.taxRate = taxRate;
                item.tax = "0";
                item.cityTax = "0";
                if (!item.discountRate) {
                    item.discountRate = '';
                }
                if (!item.totalDiscount) {
                    item.totalDiscount = '';
                }

                subtotalBeforeDC += item.price * item.quantity;
                var lineTotal = item.price - item.discountTotal;
                var lineDiscount = item.discountTotal;

                subtotal += lineTotal;

                if(lineDiscount != '') {
                    totalDiscount += round(lineDiscount, 2);
                }

                item.lineSubtotal = item.price;
                item.totalDiscount = lineDiscount;

                if (item.taxable == true) {
                    var lineTax = round(item.price * item.quantity * (1 - item.discountRate / 100), 2);
                    taxableAmount += lineTax;
                    item.tax = round((item.lineSubtotal - item.totalDiscount) * taxRate, 2);
                    item.cityTax = round((item.lineSubtotal - item.totalDiscount) * cityTaxRate, 2);
                    $scope.tax += item.tax;
                    $scope.cityTax += item.cityTax;
                }

                item.lineSubtotalDue = item.lineSubtotal - item.totalDiscount + Number(item.tax) + Number(item.cityTax);
                item.taxRate = taxRate;
            }

            $scope.subtotal = subtotalBeforeDC;
            $scope.totalDiscount = totalDiscount;
            $scope.total = round($scope.subtotal - $scope.totalDiscount + $scope.tax + $scope.cityTax, 2);
            if ($scope.shippingCost > 0) {
                $scope.total += round($scope.shippingCost, 2);
            }

            if ($rootScope.currentCheckout.payments && $rootScope.currentCheckout.payments.length > 0) {
                var paidAmount = 0;
                for (var pi = 0; pi < $rootScope.currentCheckout.payments.length; pi++) {
                    var payment = $rootScope.currentCheckout.payments[pi];
                    if (payment) {
                        paidAmount += payment.amount;
                        $scope.total -= payment.amount;
                    }
                }
                $scope.paidAmount = paidAmount;
                $scope.total = round($scope.total, 2);
            }
            $scope.paymentDue = $scope.total;
            $scope.subtotalBeforeDC = subtotalBeforeDC;

            $scope.preventEdit = ($rootScope.currentCheckout != null && $rootScope.currentCheckout.payments != null && $rootScope.currentCheckout.payments.length > 0) || $scope.subtotal <= 0;
        }
    }

    initializeCurrentCheckout($rootScope);

    $scope.paymentProcessingStatus = "";
    $scope.currentImgUrl = "";
    $scope.paymentProcessingStatus = "";
    $scope.total = "";
    $scope.subtotal = 0;
    $scope.discountRate = "";
    $scope.isTyping = false;
    $scope.paymentDue = 0;

    $scope.flipIsTyping = function () {
        if ($scope.itemNumber && $scope.itemNumber.length == 1) {
            $scope.lastTyped = new Date().getTime();
        }

        var now = new Date().getTime();

        if (now - $scope.lastTyped > 300 && $scope.itemNumber.length > 1) {
            $scope.isTyping = true;
        }
        else {
            $scope.isTyping = false;
            if ($scope.itemNumber && $scope.itemNumber.length == 0) {
                $scope.itemNumber2 = "";
            }
        }
    };

    $scope.selectItem = function (product) {
        $scope.currentImgUrl = product.imageUrl;
    };

    $scope.toggleShippingCost = function () {
        if ($rootScope.currentCheckout.items && $rootScope.currentCheckout.items.length > 0) {
            var addShippingCost = false;
            for (var i = 0; i < $rootScope.currentCheckout.items.length; i++) {
                var prod = $rootScope.currentCheckout.items[i];

                if (prod.pickupOption == 'Ship') {
                    addShippingCost = true;
                    break;
                }
            }

            if (addShippingCost && $rootScope.currentCheckout.customer.id > 0) {
                if (addShippingCost) {
                    $scope.captureShippingCost = true;
                }
                else {
                    $scope.captureShippingCost = false;
                    $scope.shippingCost = 0;
                }
            }
        }
    };

    calculateSubtotal();
    $scope.toggleShippingCost();

    if ($rootScope.currentCheckout && $rootScope.currentCheckout.discountRate) {
        $scope.discountRate = $rootScope.currentCheckout.discountRate;
    }

    $scope.updateTotalWithDC = function (product, method, validateMaxDiscount) {
        var dcRate = 0;
        if (method == 'rate') {
            dcRate = Number(product.discountRate);
            if(dcRate != product.discountPrevRate) {
                product.discountTotal = dcRate == 0 ? '' : round(dcRate * product.price / 100, 2);
                product.discountPrevRate = dcRate;
            }
        }
        else {
            dcRate = round(product.discountTotal / product.price * 100, 2);
            product.discountPrevRate = dcRate;
            product.discountRate = dcRate;
        }

        if (validateMaxDiscount && dcRate > product.maxDiscount) {
            product.discountRate = '';
            product.discountTotal = '';
            calcFinalItemPrice(product);

            calculateSubtotal();

            $scope.popupMsg = "MAX Discount Rate allowed for this brand is [" + product.maxDiscount + "].";
            ngDialog.open({scope: $scope});

            return;
        }

        if (validateMaxDiscount && round(dcRate, 2) > $rootScope.user.maxDiscount) {
            if (!$rootScope.user.manager) {
                $rootScope.needManagerApproval = true;
            }

            calcFinalItemPrice(product);
            calculateSubtotal();
        }
        else {
            calcFinalItemPrice(product);
            calculateSubtotal();
        }
    };

    function calcFinalItemPrice(item) {
        var taxRate = $rootScope.storeProfile.taxRate;
        var cityTaxRate = $rootScope.storeProfile.cityTaxRate;
        //var lineSubTotal = item.price * item.quantity - item.discountTotal;

        var discountTotal = item.discountTotal == '' ? 0:item.discountTotal;
        item.finalPrice = round(item.price * item.quantity - discountTotal, 2);
        if(item.finalPrice == 0) {
            item.finalPrice = '';
        }

        if(item.taxable) {
            item.tax = round(item.finalPrice * taxRate, 2);
            item.cityTax = round(item.finalPrice * cityTaxRate, 2);
            item.otdPrice = round(item.finalPrice + item.tax + item.cityTax,2);
        }
        else {
            item.otdPrice = (item.finalPrice) == 0 ? '': round(item.finalPrice,2);
        }

        if(item.otdPrice == 0) {
            item.otdPrice = '';
        }
    }

    $scope.calculateFinalPrice = function (product,validateMaxDiscount) {
        if(product.finalPrice < 0 ) {
            $scope.popupMsg = "Final price can't be less than 0";
            ngDialog.open({scope: $scope});
            product.finalPrice = product.price - product.discountTotal;
            return;
        }
        product.discountTotal = round(product.price - product.finalPrice,2);
        $scope.updateTotalWithDC(product,'price',validateMaxDiscount);
    };

    $scope.calculateByOTDPrice = function (product,validateMaxDiscount) {
        var taxRate = $rootScope.storeProfile.taxRate;
        var cityTaxRate = $rootScope.storeProfile.cityTaxRate;

        if ($rootScope.currentCheckout && $rootScope.currentCheckout.items.length > 0) {
            for (var i = 0; i < $rootScope.currentCheckout.items.length; i++) {
                var item = $rootScope.currentCheckout.items[i];
                if (item.forceUntaxable == true) {
                    taxRate = 0.0;
                    cityTaxRate = 0.0;
                }
            }
        }

        // calculate discount total
        if (product.taxable == true) {
            product.discountTotal = round(product.price * product.quantity - (product.otdPrice / (1 + taxRate + cityTaxRate)), 2);
        }
        else {
            product.discountTotal = product.price * product.quantity - product.otdPrice;
        }

        $scope.updateTotalWithDC(product,'price',validateMaxDiscount);
    };

    $scope.decrementQuantity = function (product) {
        var index = $rootScope.currentCheckout.items.indexOf(product);
        if (index != -1) {
            if (product.pickupOption == 'Ship') {
                $scope.shippingCost = 0;
            }
            $rootScope.currentCheckout.items.splice(index, 1);
        }

        calculateSubtotal();

        $scope.currentImgUrl = "";
        $scope.warningMsg = "";

        $scope.captureShippingCost = false;
        for (var i = 0; i < $rootScope.currentCheckout.items.length; i++) {
            var item = $rootScope.currentCheckout.items[i];

            if (item.pickupOption == 'Ship') {
                $scope.captureShippingCost = true;
            }
            if (item.price > 500) {
                $scope.warningMsg = "Item over $500.";
                $scope.$apply();
            }
        }
    };

    $scope.lookupItem = function (itemNumber) {
        var executeAPI = false;

        if ($scope.isTyping) {
            // only when both numbers are entered.
            if ($scope.itemNumber && $scope.itemNumber2 && $scope.itemNumber == $scope.itemNumber2) {
                executeAPI = true;
            }
            else {
                if ($scope.itemNumber2 && $scope.itemNumber != $scope.itemNumber2) {
                    document.getElementById("itemNumber2").focus();
                    $scope.popupMsg = "You typed wrong Item SKU. Verify and try again.";
                    ngDialog.open({scope: $scope});
                }
                else {
                    document.getElementById("itemNumber2").focus();
                }
                return;
            }
        }
        else {
            executeAPI = true;
        }

        if (executeAPI) {
            var url = getURL($rootScope) + "/flow/POS.Item.LookupByNumber";

            $http.get(url, {params: {'item': itemNumber, 'storeId': $rootScope.user.storeId}})
                .success(function (data, status, headers, config) {
                    if (data && data.id > 0) {
                        var product = data;
                        var qtyOnCart = getQtyOnCart(product.sku);
                        if(qtyOnCart >= product.atp) {
                            $scope.popupMsg = "[" + product.sku + "] is not available";
                            ngDialog.open({scope: $scope});
                            return;
                        }
                        product.discountTotal = '';
                        product.discountRate = '';
                        product.discountPrevRate = 0;
                        calcFinalItemPrice(product);

                        $rootScope.currentCheckout.items.push(product);
                        calculateSubtotal();

                        $scope.itemNumber = "";
                        $scope.itemNumber2 = "";
                        $scope.itemNumber = "";
                        $scope.isTyping = false;
                        // Focus on the Item # for next item
                        if (product.price > 500 && $rootScope.currentCheckout.customer.id == undefined) {
                            $scope.warningMsg = "Item over $500.";
                        }
                        else {
                            $scope.warningMsg = '';
                        }

                        $scope.currentImgUrl = product.imageUrl;

                        document.getElementById("itemNumber").focus();
                    }
                    else {
                        $scope.popupMsg = data;
                        ngDialog.open({scope: $scope});
                    }
                })
                .error(function (data, status, headers, config) {
                    $scope.popupMsg = "No item by [" + itemNumber + "]";
                    ngDialog.open({scope: $scope});
                }
            );
        }
    };


    function getQtyOnCart(sku) {
        var qtyOnCart=0;
        if ($rootScope.currentCheckout && $rootScope.currentCheckout.items.length > 0) {
            for (var i = 0; i < $rootScope.currentCheckout.items.length; i++) {
                var item = $rootScope.currentCheckout.items[i];
                if(item.sku == sku) {
                    qtyOnCart += item.quantity;
                }
            }
        }
        return qtyOnCart;
    }

    $scope.lookupCustomer = function (phoneNumber) {
        if (phoneNumber) {
            var url = getURL($rootScope) + "/flow/POS.Customer.LookupBy";
            phoneNumber = phoneNumber.replace(/-/g, "");

            $http.get(url, {params: {'by': 'phone', 'value': phoneNumber}})
                .success(function (data, status, headers, config) {
                    if (data) {
                        var customer = data;

                        $rootScope.currentCheckout.customer = customer;
                        $scope.warningMsg = '';
                        $scope.toggleShippingCost();
                    }
                })
                .error(function (data, status, headers, config) {
                    $scope.popupMsg = "No customer found";
                    ngDialog.open({scope: $scope});
                }
            );
            $rootScope.phoneNumber = phoneNumber;
        }
    };

    $scope.saveTrx = function () {
        $scope.dialogMsg = "Do you want to save this order for later? You can find the saved orders under 'Orders' Menu";

        var checkoutDialog = ngDialog.open({template: 'templates/confirm-dialog.html', scope: $scope, controller: 'ConfirmDialogCtrl'});
        checkoutDialog.closePromise.then(function (data) {
            var returnValue = data.value;

            var customerId;
            if($rootScope.currentCheckout.customer) {
                customerId = $rootScope.currentCheckout.customer.id;
            }
            if (returnValue == 1) {
                var transact = {
                    "user": $rootScope.user,
                    "amount": $scope.payAmount,
                    "totals": {
                        "total": $scope.total,
                        "subtotal": $scope.subtotal,
                        "totalDiscount": $scope.totalDiscount,
                        "tax": $scope.tax,
                        "cityTax": $scope.cityTax
                    },
                    "lines": $rootScope.currentCheckout.items,
                    "customerId": customerId,
                    "salesId": $rootScope.currentCheckout.salesId,
                    "notes": $scope.notes
                };

                var url = getURL($rootScope) + "/flow/POS.Sales.SaveAsDraft";
                $http.post(url, transact).
                    success(function (data, status, headers, config) {
                        $scope.resetTrx();

                        $scope.paymentProcessingStatus = "SAVED";
                    }).
                    error(function (data, status, headers, config) {
                        $scope.paymentProcessingStatus = data;
                    }
                );
            }
        });
    };

    $scope.calculateCost = function () {
        var storeId = $rootScope.user.storeId;
        var shippingItems = [];
        for (var i = 0; i < $rootScope.currentCheckout.items.length; i++) {
            var item = $rootScope.currentCheckout.items[i];
            if (item.pickupOption == 'Ship') {
                shippingItems.push(item.id);
                storeId = item.storeId;
            }
        }

        var combinedItemWeight = 0;
        if (shippingItems.length > 0) {
            $http.post(getURL($rootScope) + '/flow/POS.Item.GetCombinedWeight', shippingItems)
                .success(function (data, status, config, headers) {
                    combinedItemWeight = data;
                }
            );
        }

        $scope.shippingDTO = {
            "weight": combinedItemWeight,
            "storeId": storeId,
            "storeName": $rootScope.user.storeName,
            "userId": $rootScope.user.id,
            "boxes": [],
            "customer": {}
        };

        if ($rootScope.currentCheckout.customer.id > 0) {
            $scope.shippingDTO.customer = $rootScope.currentCheckout.customer
        }
        $http.get(getURL($rootScope) + '/flow/POS.Item.GetShippingBoxes', {'params': {'storeId': storeId}})
            .success(function (data, status, header, config) {
                if (data) {
                    for (var i = 0; i < data.length; i++) {
                        var box = data[i];
                        $scope.shippingDTO.boxes.push(box);
                    }
                }
            }
        );

        var rateShop = ngDialog.open({template: 'templates/shipping.html', scope: $scope, controller: 'ShippingCtrl', className: 'ngdialog-theme-mine'});
        rateShop.closePromise.then(function (data) {
            var allowEdit = true;
            var returnValue = data.value;

            if (returnValue) {
                if (returnValue.shippingCost > 0) {
                    $scope.shippingCost = returnValue.shippingCost;
                    $scope.shipMethod = returnValue.shipMethod;
                    $scope.carrier = returnValue.carrier;

                    calculateSubtotal();
                }
            }

            $scope.shippingDTO = {};
        });

    };

    $scope.detachCustomerFromTrx = function () {
        $rootScope.currentCheckout.customer = {};
        $scope.phoneNumber = "";
        $scope.captureShippingCost = false;
    };

    $scope.displayProductImage = function (product) {
        $scope.productImage = product.imageUrl;
        ngDialog.open({template: 'templates/display-image.html', scope: $scope, className: 'ngdialog-theme-mine'});
    };

    $scope.updateOnItemPrice = function () {
        calculateSubtotal();
    };

    $scope.viewCustomerDetail = function (customerId) {
        if (customerId > 0) {
            /* Converts an object into a key/value par with an optional prefix. Used for converting objects to a query string */
            var searchParams = {
                'cid': customerId,
                'phoneNumber': $scope.phoneNumber,
                'email': $scope.email,
                'customerName': $scope.customerName,
                'dob': $scope.dob,
                'zipCode': $scope.zipCode,
                'driverLicense': $scope.driverLicense,
                'fromPage': "checkout"
            };

            var queryString = qs(searchParams);
            $location.path('/customer_detail').search(queryString);
        }
    };

    var orderId = $location.search().orderId;

    if (orderId !== undefined && orderId > 0) {
        $http.get(getURL($rootScope) + '/flow/POS.Transact.LoadPendingOrder', {params: {'orderId': orderId}})
            .success(function (data, status, headers, config) {
                $rootScope.currentCheckout.items = data.items;
                $rootScope.currentCheckout.customer = data.customer;
                $rootScope.currentCheckout.salesId = data.salesId;
                $scope.notes = data.notes;

                for (var i = 0; i < $rootScope.currentCheckout.items.length; i++) {
                    var item = $rootScope.currentCheckout.items[i];
                    calcFinalItemPrice(item);
                }

                calculateSubtotal();
            }).error(function (data, status, headers, config) {
                $scope.popupMsg = "Unable to load the pending order [" + orderId + "]";
                ngDialog.open({scope: $scope});
            }
        );
    }
    $rootScope.phoneNumber = '';
    $location.$$search = {};
});