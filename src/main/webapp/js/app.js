'use strict';

angular.module('ngIOS9UIWebViewPatch', ['ng']).config(['$provide', function($provide) {
    'use strict';

    $provide.decorator('$browser', ['$delegate', '$window', function($delegate, $window) {

        if (isIOS9UIWebView($window.navigator.userAgent)) {
            return applyIOS9Shim($delegate);
        }

        return $delegate;

        function isIOS9UIWebView(userAgent) {
            return /(iPhone|iPad|iPod).* OS 9_\d/.test(userAgent) && !/Version\/9\./.test(userAgent);
        }

        function applyIOS9Shim(browser) {
            var pendingLocationUrl = null;
            var originalUrlFn= browser.url;

            browser.url = function() {
                if (arguments.length) {
                    pendingLocationUrl = arguments[0];
                    return originalUrlFn.apply(browser, arguments);
                }

                return pendingLocationUrl || originalUrlFn.apply(browser, arguments);
            };

            window.addEventListener('popstate', clearPendingLocationUrl, false);
            window.addEventListener('hashchange', clearPendingLocationUrl, false);

            function clearPendingLocationUrl() {
                pendingLocationUrl = null;
            }

            return browser;
        }
    }]);
}]);

var posApp = angular.module('myPos', [
    'ngRoute',
    'posCloseOutControllers',
    'posGlobalControllers',
    'posTrxControllers',
    'posCustomerControllers',
    'posProductControllers',
    'posSetupControllers',
    'posSalesControllers',
    'posGiftControllers',
    'ngCookies',
    'ngTouch',
    'ngDialog',
    'mm.foundation',
    'ngIOS9UIWebViewPatch'
]).run(function () {
    FastClick.attach(document.body);
}).filter('tel', function () {
    return function (tel) {
        if (!tel) {
            return '';
        }

        var value = tel.toString().trim().replace(/^\+/, '');

        if (value.match(/[^0-9]/)) {
            return tel;
        }

        var country, city, number;

        switch (value.length) {
            case 10: // +1PPP####### -> C (PPP) ###-####
                country = 1;
                city = value.slice(0, 3);
                number = value.slice(3);
                break;

            case 11: // +CPPP####### -> CCC (PP) ###-####
                country = value[0];
                city = value.slice(1, 4);
                number = value.slice(4);
                break;

            case 12: // +CCCPP####### -> CCC (PP) ###-####
                country = value.slice(0, 3);
                city = value.slice(3, 5);
                number = value.slice(5);
                break;

            default:
                return tel;
        }

        if (country == 1) {
            country = "";
        }

        number = number.slice(0, 3) + '-' + number.slice(3);

        return (country + " (" + city + ") " + number).trim();
    };
}).filter('startFrom', function () {
    return function (input, start) {
        if (input) {
            start = +start;
            return input.slice(start);
        }
    };
});

posApp.config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.headers['Content-Type'] = 'text/plain';
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
}
]);

posApp.config(['$sceDelegateProvider', function ($sceDelegateProvider) {
    $sceDelegateProvider.resourceUrlWhitelist(['self', 'http://pos-ua.deposco.com/**', 'http://pos.deposco.com/**', 'http://*.deposco.com/**', 'http://localhost:9090/**']);
}]);

posApp.config(['ngDialogProvider', function (ngDialogProvider) {
    ngDialogProvider.setDefaults({
        className: 'ngdialog-theme-default',
        plain: false,
        template: 'templates/simple-dialog.html',
        showClose: false,
        closeByDocument: false,
        closeByEscape: false,
        name: 'SimplePopup',
        cache: false
    });
}]);

posApp.directive('roundNumber', function(){
    return {
        require: 'ngModel',
        link: function(scope, element, attrs, modelCtrl) {

            modelCtrl.$parsers.push(function (inputValue) {

                var transformedInput = round(inputValue,2)

                if(transformedInput == 0) return '';

                if (transformedInput!=inputValue) {
                    modelCtrl.$setViewValue(transformedInput);
                    modelCtrl.$render();
                }

                return transformedInput;
            });
        }
    };
});

function round(value, exp) {
    if (typeof exp === 'undefined' || +exp === 0) {
        return Math.round(value);
    }

    value = +value;
    exp = +exp;

    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
        return NaN;
    }

    // Shift
    value = value.toString().split('e');
    value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
}

posApp.directive('keyBind', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if (event.which === Number(attrs.key)) {
                scope.$apply(function () {
                    scope.$eval(attrs.keyBind, {'event': event, 'element': element});
                });

                event.preventDefault();
            }
        });
    };
});

posApp.directive("tabs", function () {
    return {
        restrict: "E",
        transclude: true,
        scope: {},
        controller: function ($scope, $element) {
            var panes = $scope.panes = [];

            $scope.select = function (pane) {
                angular.forEach(panes, function (pane) {
                    pane.selected = false;
                });
                if (pane.load !== undefined) {
                    pane.load();
                }
                pane.selected = true;
            };

            this.addPane = function (pane) {
                if (panes.length === 0) $scope.select(pane);
                panes.push(pane);
            };
        },
        template: '<div class="tabbable">' +
        '<ul class="nav nav-tabs">' +
        '<li ng-repeat="pane in panes" ng-class="{active:pane.selected}">' +
        '<a href="" ng-click="select(pane)">{{pane.tabTitle}}</a>' +
        '</li>' +
        '</ul>' +
        '<div class="tab-content" ng-transclude></div>' +
        '</div>',
        replace: true
    };
});

posApp.directive("pane", ["$http", "$templateCache", "$controller", "$compile", function ($http, $templateCache, $controller, $compile) {
    return {
        require: "^tabs",
        restrict: "E",
        transclude: true,
        scope: {tabTitle: "@"},
        link: function (scope, element, attrs, tabsCtrl) {
            var templateCtrl, templateScope;

            if (attrs.template && attrs.controller) {
                scope.load = function () {
                    $http.get(attrs.template, {cache: $templateCache})
                        .then(function (response) {
                            templateScope = scope.$new();
                            templateScope.isTabbedPane = true;
                            templateCtrl = $controller(attrs.controller, {$scope: templateScope});
                            element.html(response.data);
                            element.children().data('$ngControllerController', templateCtrl);
                            $compile(element.contents())(templateScope);
                        });
                };
            }

            tabsCtrl.addPane(scope);
        },
        template: '<div class="tab-pane" ng-class="{active: selected}" ng-transclude>' +
        '</div>',
        replace: true
    };
}]);

//
//// Common directive for Focus
//
//posApp.directive('focus',
//    function ($timeout) {
//        return {
//            scope: {
//                trigger: '@focus'
//            },
//            link: function (scope, element) {
//                scope.$watch('trigger', function (value) {
//                    if (value === "true") {
//                        $timeout(function () {
//                            element[0].focus();
//                        }, 100);
//                    }
//                });
//            }
//        };
//    }
//);

posApp.directive("drawing", function () {
    return {
        restrict: "A",
        link: function (scope, element) {
            var ctx = element[0].getContext('2d');

            // variable that decides if something should be drawn on mousemove
            var drawing = false;

            // the last coordinates before the current move
            var lastX;
            var lastY;
            var currentX;
            var currentY;

            element.bind('touchstart', function (event) {
                if (event.offsetX !== undefined) {
                    lastX = event.offsetX;
                    lastY = event.offsetY;
                } else { // Firefox compatibility
                    lastX = event.layerX - event.currentTarget.offsetLeft;
                    lastY = event.layerY - event.currentTarget.offsetTop;
                }

                // begins new line
                ctx.beginPath();
                drawing = true;
            });
            element.bind('mousedown', function (event) {
                if (event.offsetX !== undefined) {
                    lastX = event.offsetX;
                    lastY = event.offsetY;
                } else { // Firefox compatibility
                    lastX = event.layerX - event.currentTarget.offsetLeft;
                    lastY = event.layerY - event.currentTarget.offsetTop;
                }

                // begins new line
                ctx.beginPath();
                drawing = true;
            });

            element.bind('touchmove', function (event) {
                if (drawing) {
                    // get current mouse position
                    if (event.offsetX !== undefined) {
                        currentX = event.offsetX;
                        currentY = event.offsetY;
                    } else {
                        currentX = event.layerX - event.currentTarget.offsetLeft;
                        currentY = event.layerY - event.currentTarget.offsetTop;
                    }

                    draw(lastX, lastY, currentX, currentY);

                    // set current coordinates to last one
                    lastX = currentX;
                    lastY = currentY;
                }
            });
            element.bind('mousemove', function (event) {
                if (drawing) {
                    // get current mouse position
                    if (event.offsetX !== undefined) {
                        currentX = event.offsetX;
                        currentY = event.offsetY;
                    } else {
                        currentX = event.layerX - event.currentTarget.offsetLeft;
                        currentY = event.layerY - event.currentTarget.offsetTop;
                    }

                    draw(lastX, lastY, currentX, currentY);

                    // set current coordinates to last one
                    lastX = currentX;
                    lastY = currentY;
                }
            });

            element.bind('touchend', function (event) {
                // stop drawing
                drawing = false;
            });
            element.bind('mouseup', function (event) {
                // stop drawing
                drawing = false;
            });

            // canvas reset
            function reset() {
                element[0].width = element[0].width;
            }

            function draw(lX, lY, cX, cY) {
                // line from
                ctx.moveTo(lX, lY);
                // to
                ctx.lineTo(cX, cY);
                // color
                ctx.strokeStyle = "#001a28";
                // draw it
                ctx.stroke();
            }
        }
    };
});

posApp.config(['$routeProvider', '$httpProvider',
        function ($routeProvider, $httpProvider) {
            $routeProvider.
                when('/checkout', {
                    templateUrl: 'partials/trx.html',
                    controller: 'TransactionCtrl'
                }).
                when('/postauth', {
                    templateUrl: 'partials/postauth.html',
                    controller: 'LoginCtrl'
                }).
                when('/customers', {
                    templateUrl: 'partials/customers.html',
                    controller: 'CustomerCtrl'
                }).
                when('/new_customer', {
                    templateUrl: 'partials/new_customer.html',
                    controller: 'NewCustomerCtrl'
                }).
                when('/customer_detail', {
                    templateUrl: 'partials/customer_detail.html',
                    controller: 'CustomerDetailCtrl'
                }).
                when('/products', {
                    templateUrl: 'partials/products.html',
                    controller: 'ProductCtrl'
                }).
                when('/login', {
                    templateUrl: 'partials/login.html',
                    controller: 'LoginCtrl'
                }).
                when('/setup', {
                    templateUrl: 'partials/setup.html',
                    controller: 'SetupCtrl'
                }).
                when('/gift', {
                    templateUrl: 'partials/gift-cards.html',
                    controller: 'GiftCtrl'
                }).
                when('/closeOut', {
                    templateUrl: 'partials/closeOut.html',
                    controller: 'CloseOutCtrl'
                }).
                when('/closeOutDetail', {
                    templateUrl: 'partials/closeOutDetail.html',
                    controller: 'CloseOutDetailCtrl'
                }).
                when('/orders', {
                    templateUrl: 'partials/orders.html',
                    controller: 'OrderCtrl'
                }).
                when('/orders/:customerId', {
                    templateUrl: 'partials/orders.html',
                    controller: 'OrderCtrl'
                }).
                when('/orderdetail', {
                    templateUrl: 'partials/orderdetail.html',
                    controller: 'OrderDetailCtrl'
                }).
                when('/orderdetail/:orderId', {
                    templateUrl: 'partials/orderdetail.html',
                    controller: 'OrderDetailCtrl'
                }).
                otherwise({
                    redirectTo: '/postauth'
                }
            );

            $httpProvider.interceptors.push('authInterceptor');
        }
    ]
);

posApp.factory('authInterceptor', function ($rootScope, $q, $window, $cookieStore) {
        return {
            request: function (config) {
                config.headers = config.headers || {};
                var sessionKey = $cookieStore.get('sessionKey');

                if (sessionKey) {
                    config.headers.sessionKey = sessionKey;
                }

                config.headers['Content-Type'] = "text/plain";

                return config;
            },
            responseError: function (response) {
                if (response && response.status === 401) {
                    $rootScope.sessionKey = null;
                    console.log($rootScope.loginUrl);

                    $window.location.href = $rootScope.loginUrl + "/#login?msg=Session expired";
                    return;
                }
                //if (response && response.status >= 500) {
                //}
                else {
                    return $q.reject(response);
                }
            }
        };
    }
);
