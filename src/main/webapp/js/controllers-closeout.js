'use strict';

var posControllers = angular.module('posCloseOutControllers', []);

posControllers.controller('CloseOutCtrl', function ($rootScope, $scope, $http, ngDialog) {
    $scope.currentPage = 0;
    $scope.pageSize = 8;
    $scope.numberOfPages = 0;

    $scope.searchCloseOut = function () {
        var url = getURL($rootScope) + "/flow/POS.CloseOut.FindCloseOut";

        if (!$scope.startDate) {
            $scope.popupMsg = "Search start date is not selected";
            ngDialog.open({scope: $scope});
            return;
        }
        if (!$scope.endDate) {
            $scope.endDate = '';
        }
        $http.get(url, {params: {
            'storeId': $rootScope.user.storeId,
            'startDate': $scope.startDate,
            'endDate': $scope.endDate
        }}).success(function (data, status, headers, config) {
            if (data) {
                $scope.closeOuts = data;

                $scope.numberOfPages = function () {
                    if ($scope.closeOuts) {
                        return Math.ceil($scope.closeOuts.length / $scope.pageSize);
                    }

                    return 1;
                }
            }
        }).error(function (data, status, headers, config) {
            $scope.closeOuts = [];
        });
    };

    $scope.viewCloseOutDetail = function (closeOutId) {
        if (closeOutId > 0) {
            $location.path('/closeOutDetail');
        }
    }

});

posControllers.controller('CloseOutDetailCtrl', function ($rootScope, $scope, $routeParams, $http, $cookieStore, ngDialog) {

    $scope.init = function () {
        return {beginPennies: '', beginNickels: '', beginDimes: '', beginQuarters: '', beginOnes: '', beginFives: '', beginTens: '', beginTwenties: '', beginFifties: '', beginHundreds: '',
            endPennies: '', endNickels: '', endDimes: '', endQuarters: '', endOnes: '', endFives: '', endTens: '', endTwenties: '', endFifties: '', endHundreds: '', cashPaidIn: '', cashPaidOut: ''};
    };

    $scope.formatDate = function (date) {
        var dd = date.getDate();
        var mm = date.getMonth() + 1; //January is 0!

        var yyyy = date.getFullYear();
        if (dd < 10) {
            dd = '0' + dd;
        }
        if (mm < 10) {
            mm = '0' + mm;
        }

        return yyyy + '-' + mm + '-' + dd;
    };

    $scope.loadCloseOut = function (closeOutDate) {

        var closeOutDateStr = $scope.formatDate(closeOutDate);

        var url = getURL($rootScope) + "/flow/POS.CloseOut.FindCloseOut";

        if (!closeOutDateStr) {
            $scope.popupMsg = "Search date is not selected";
            ngDialog.open({scope: $scope});
            return;
        }

        $http.get(url, {params: {
            'storeId': $rootScope.user.storeId,
            'startDate': closeOutDateStr,
            'endDate': closeOutDateStr
        }}).success(function (data, status, headers, config) {
            if (data) {
                if(data.length == 0) {
                    $scope.closeOut = $scope.init();
                    $scope.closeOut.closeOutDateStr = closeOutDateStr;
                }
                else {
                    $scope.closeOut = data[0];
                }
            }
        }).error(function (data, status, headers, config) {
            //
        });
    };

    $scope.stripZero = function (value) {
        value = value == 0 ? '':value;
    };

    $scope.closeOut = $scope.init();

    var today = new Date();

    $scope.closeOut.closeOutDateStr = today;

    var closeOutId = $routeParams.closeOutId;

    if (closeOutId != null) {
        var url = getURL($rootScope) + "/flow/POS.CloseOut.GetCloseOut";

        $http.get(url, {params: {
            'storeId': $rootScope.user.storeId,
            'closeOutId': closeOutId
        }}).success(function (data, status, headers, config) {
            if (data) {
                $scope.closeOut = data;
            }
        }).error(function (data, status, headers, config) {
            $scope.closeOuts = {};
        });
    }
    else {
        $scope.loadCloseOut(today);
    }

    $scope.goBack = function () {
        window.history.back();
    };

    $scope.openCashRegister = function () {
        openCashRegister();
    };

    $scope.strip = function (value) {
        var value = round(value, 2);
        return value.formatMoney(2, '.', ',');
    };

    $scope.getBeginTotal = function (closeOut) {
        return closeOut.beginPennies * 0.01 + closeOut.beginNickels * 0.05 + closeOut.beginDimes * 0.1 + closeOut.beginQuarters * 0.25 + closeOut.beginOnes * 1 + closeOut.beginFives * 5 + closeOut.beginTens * 10 + closeOut.beginTwenties * 20 + closeOut.beginFifties * 50 + closeOut.beginHundreds * 100
    };

    $scope.getEndTotal = function (closeOut) {
        return closeOut.endPennies * 0.01 + closeOut.endNickels * 0.05 + closeOut.endDimes * 0.1 + closeOut.endQuarters * 0.25 + closeOut.endOnes * 1 + closeOut.endFives * 5 + closeOut.endTens * 10 + closeOut.endTwenties * 20 + closeOut.endFifties * 50 + closeOut.endHundreds * 100;
    };

    $scope.getDollarsNet = function (closeOut) {
        return $scope.getBeginTotal(closeOut) + closeOut.cashPaidIn - closeOut.cashPaidOut;
    };

    $scope.update = function (closeout) {

        if(closeout.beginPennies == null || closeout.beginPennies.length == 0) {
            closeout.beginPennies = 0;
        }
        if(closeout.beginDimes == null || closeout.beginDimes.length == 0) {
            closeout.beginDimes = 0;
        }
        if(closeout.beginNickels == null || closeout.beginNickels.length == 0) {
            closeout.beginNickels = 0;
        }
        if(closeout.beginPennies == null || closeout.beginPennies.length == 0) {
            closeout.beginPennies = 0;
        }
        if(closeout.beginQuarters == null || closeout.beginQuarters.length == 0) {
            closeout.beginQuarters = 0;
        }
        if(closeout.beginOnes == null || closeout.beginOnes.length == 0) {
            closeout.beginOnes = 0;
        }
        if(closeout.beginFives == null || closeout.beginFives.length == 0) {
            closeout.beginFives = 0;
        }
        if(closeout.beginTens == null || closeout.beginTens.length == 0) {
            closeout.beginTens = 0;
        }
        if(closeout.beginTwenties == null || closeout.beginTwenties.length == 0) {
            closeout.beginTwenties = 0;
        }
        if(closeout.beginFifties == null || closeout.beginFifties.length == 0) {
            closeout.beginFifties = 0;
        }
        if(closeout.beginHundreds == null || closeout.beginHundreds.length == 0) {
            closeout.beginHundreds = 0;
        }
        if(closeout.endPennies == null || closeout.endPennies.length == 0) {
            closeout.endPennies = 0;
        }
        if(closeout.endDimes == null || closeout.endDimes.length == 0) {
            closeout.endDimes = 0;
        }
        if(closeout.endNickels == null || closeout.endNickels.length == 0) {
            closeout.endNickels = 0;
        }
        if(closeout.endPennies == null || closeout.endPennies.length == 0) {
            closeout.endPennies = 0;
        }
        if(closeout.endQuarters == null || closeout.endQuarters.length == 0) {
            closeout.endQuarters = 0;
        }
        if(closeout.endOnes == null || closeout.endOnes.length == 0) {
            closeout.endOnes = 0;
        }
        if(closeout.endFives == null || closeout.endFives.length == 0) {
            closeout.endFives = 0;
        }
        if(closeout.endTens == null || closeout.endTens.length == 0) {
            closeout.endTens = 0;
        }
        if(closeout.endTwenties == null || closeout.endTwenties.length == 0) {
            closeout.endTwenties = 0;
        }
        if(closeout.endFifties == null || closeout.endFifties.length == 0) {
            closeout.endFifties = 0;
        }
        if(closeout.endHundreds == null || closeout.endHundreds.length == 0) {
            closeout.endHundreds = 0;
        }

        if(closeout.cashPaidIn == null || closeout.cashPaidIn.length == 0) {
            closeout.cashPaidIn = 0;
        }
        if(closeout.cashPaidOut == null || closeout.cashPaidOut.length == 0) {
            closeout.cashPaidOut = 0;
        }

        if(closeout.leaveTotal == null || closeout.leaveTotal.length == 0) {
            closeout.leaveTotal = 0;
        }

        if(closeout.depositTotal == null || closeout.depositTotal.length == 0) {
            closeout.depositTotal = 0;
        }
        if(closeout.depositTotal > $scope.getEndTotal(closeout)) {
            $scope.popupMsg = "Deposit amount "+closeout.depositTotal+" is greater than total counted amount " + $scope.getEndTotal(closeout);
            ngDialog.open({scope: $scope});
            return;
        }

        if (closeout.id) {
            $scope.dialogMsg = "Are you sure to update current close out ?";
    
            var checkoutDialog = ngDialog.open({template: 'templates/confirm-dialog.html', scope: $scope, controller: 'ConfirmDialogCtrl'});
            checkoutDialog.closePromise.then(function (data) {
                var returnValue = data.value;

                if (returnValue == 1) {
                    $http.put(getURL($rootScope) + '/flow/POS.CloseOut.UpdateCloseOut?storeId=' + $rootScope.user.storeId, closeout)
                        .success(function (data, status, headers, config) {
                            $scope.closeOut = data;
                            $scope.popupMsg = "Current close out has been updated";
                            ngDialog.open({scope: $scope});
                        })
                        .error(function (data, status, headers, config) {
                            $scope.popupMsg = "Error: " + data;
                            ngDialog.open({scope: $scope});
                        }
                    );
                }
            });
        }
        else {
            $http.post(getURL($rootScope) + '/flow/POS.CloseOut.CreateNewCloseOut?storeId=' + $rootScope.user.storeId, closeout)
                .success(function (data, status, headers, config) {
                    $scope.closeOut = data;
                    $scope.popupMsg = "New Close Out has been created";
                    ngDialog.open({scope: $scope});
                })
                .error(function (data, status, headers, config) {
                    $scope.popupMsg = "Error: " + data;
                    ngDialog.open({scope: $scope});
                }
            );
        }
    };

    $scope.updateLeave = function (closeOut) {
        var countTotal = $scope.getEndTotal(closeOut);
        closeOut.leaveTotal = countTotal - closeOut.depositTotal;
    };

    $scope.updateDeposit = function (closeOut) {
        var countTotal = $scope.getEndTotal(closeOut);
        closeOut.depositTotal = countTotal - closeOut.leaveTotal;
    };

    $scope.close = function (closeOut) {
        $http.get(getURL($rootScope) + "/flow/POS.CloseOut.GetCloseOut", {params: {'closeOutId': closeOut.id}})
            .success(function (data, status, headers, config) {
                if (data) {
                    $scope.closeOut = data;
                }
            })
            .error(function (data, status, headers, config) {
                $scope.popupMsg = data;
                ngDialog.open({scope: $scope});
            });
    };

    $scope.email = function (closeOut) {
        $http.get(getURL($rootScope) + "/flow/POS.CloseOut.EmailCloseOut", {params: {'closeOutId': closeOut.id}})
            .success(function (data, status, headers, config) {
                $scope.popupMsg = "Closeout has been sent";
                ngDialog.open({scope: $scope});
            })
            .error(function (data, status, headers, config) {
                $scope.popupMsg = data;
                ngDialog.open({scope: $scope});
            });
    };

    $scope.print = function (closeOut) {
        if (window.cordova !== undefined) {
            var printerIP = window.localStorage.getItem("printerIP");

            var sessionKey = $cookieStore.get('sessionKey');
            if (!sessionKey) {
                alert("Session key doesn't exist : " + sessionKey);
                return;
            }

            cordova.exec(
                function callback(data) {
                    console.log('Succeeded ');
                },
                // Register the errorHandler
                function errorHandler(err) {
                    $scope.paymentProcessingStatus = "Error while printing";
                },
                'StarPrinter',
                'printCloseOut',
                [getURL($rootScope), sessionKey, closeOut.id, printerIP]
            );
        }
    }
});