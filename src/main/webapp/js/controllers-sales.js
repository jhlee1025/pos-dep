'use strict';

var posControllers = angular.module('posSalesControllers', []);

posControllers.controller('OrderCtrl', function ($scope) {
    $scope.panes = [
        {
            "name": "Search Orders",
            "template": "templates/order-default-pane.html",
            "controller": "OrderDefaultCtrl",
            "includedInTabView": true
        },
        {
            "name": "Pending Orders",
            "template": "templates/order-pending-pane.html",
            "controller": "OrderPendingCtrl",
            "includedInTabView": true
        },
        {
            "name": "Recent Orders",
            "template": "templates/order-recent-pane.html",
            "controller": "OrderRecentCtrl",
            "includedInTabView": true
        },
        {
            "name": "Deliver to Store",
            "template": "templates/order-store-delivery-pane.html",
            "controller": "OrderStoreDeliveryCtrl",
            "includedInTabView": true

        }
    ];
});

posControllers.controller('OrderStoreDeliveryCtrl', function ($rootScope, $scope, $location, $http, $routeParams, ngDialog) {
    $scope.stores = $http.get(getURL($rootScope) + '/flow/POS.Sales.GetStoresToDeliverList', {'params': {'storeId': $rootScope.user.storeId, 'storeName': $rootScope.user.storeName}})
        .success(function (data, status, header, config) {
            if (data) {
                $scope.panes = [];

                for (var i = 0; i < data.length; i++) {
                    var store = data[i];

                    var storePane = {
                        'name': store,
                        'template': 'templates/order-store-delivery-main.html',
                        'controller': "OrderStoreDeliveryHeaderCtrl",
                        "includedInTabView": true
                    };

                    $scope.panes.push(storePane);
                }
            }
        })
        .error(function (data, status, header, config) {
            $scope.popupMsg = "No store delivery";
            ngDialog.open({scope: $scope});
        }
    );
});

posControllers.controller('OrderStoreDeliveryHeaderCtrl', function ($rootScope, $scope, $location, $http, $routeParams, ngDialog) {
    $scope.currentPage = 0;
    $scope.pageSize = 5;
    $scope.numberOfPages = 0;

    $http.get(getURL($rootScope) + "/flow/POS.Sales.GetStoreDeliveryItems", {params: {'storeName': $scope.$parent.tabTitle, 'storeId': $rootScope.user.storeId}})
        .success(function (data, status, headers, config) {
            if (data) {
                $scope.items = data;

                $scope.currentPage = 0;
                $scope.numberOfPages = function () {
                    if ($scope.items) {
                        return Math.ceil($scope.items.length / $scope.pageSize);
                    }

                    return 1;
                }
            }
        })
        .error(function (data, status, headers, config) {
        }
    );

    $scope.calculateLineWeight = function (item) {
        if (item.weight > 0) {
            item.totalItemWeight = round(item.weight, 2) * round(item.deliveryQty, 2);
        }
        else {
            item.totalItemWeight = 0;
        }
    };

    $scope.prepareStoreDelivery = function () {
        if ($scope.items.length > 0) {
            $scope.req = {
                items: []
            };
            $scope.req.deliveryTo = $scope.$parent.tabTitle;
            $scope.req.storeId = $rootScope.user.storeId;

            for (var i = 0; i < $scope.items.length; i++) {
                var item = $scope.items[i];

                if (item.deliveryQty > 0) {
                    $scope.req.items.push(item);
                }
            }

            if ($scope.req.items.length > 0) {
                var checkoutDialog = ngDialog.open({
                    template: 'templates/order-store-delivery-shipping.html',
                    scope: $scope,
                    controller: 'OrderStoreDeliveryShippingCtrl',
                    className: 'ngdialog-theme-mine'
                });
                checkoutDialog.closePromise.then(function (data) {
                    var returnValue = data.value;

                    $scope.req = {};

                    if (returnValue > 0) {
                        // TODO
                    }
                });
            }
        }
    };
});

posControllers.controller('OrderStoreDeliveryShippingCtrl', function ($rootScope, $scope, $location, $http, $routeParams, ngDialog) {
    $scope.currentPage = 0;
    $scope.pageSize = 5;
    $scope.numberOfPages = 0;

    $http.get(getURL($rootScope) + '/flow/POS.Item.GetShippingBoxes', {'params': {'storeId': $rootScope.user.storeId}})
        .success(function (data, status, header, config) {
            $scope.boxes = data;
        })
        .error(function (data, status, header, config) {
        }
    );
    $scope.populateBoxDimension = function (box) {
        if (box) {
            $scope.width = box.width;
            $scope.height = box.height;
            $scope.length = box.length;
            $scope.boxWeight = box.weight;

            $scope.totalWeight = $scope.boxWeight + $scope.contentWeight;
        }
    };

    $scope.calculateTotalWeight = function () {
        if ($scope.boxWeight >= 0 && $scope.contentWeight >= 0) {
            $scope.totalWeight = $scope.boxWeight + $scope.contentWeight;
        }
    };

    $scope.performRateShopping = function () {
        if ($scope.width < 0 || $scope.height < 0 || $scope.length < 0 || $scope.totalWeight < 0) {
            $scope.popupMsg = "Enter width, height, length, and total weight";
            ngDialog.open({scope: $scope});

            return;
        }

        var req = {
            'companyId': $rootScope.user.companyId,
            'company': $rootScope.user.company,
            'storeId': $rootScope.user.storeId,
            'shipTo': $scope.req.deliveryTo,
            'dimension': {
                'width': $scope.width,
                'height': $scope.height,
                'length': $scope.length,
                'weight': $scope.totalWeight
            }
        };

        $http.post(getURL($rootScope) + '/flow/POS.Store.Get Rate Shopping Results', req)
            .success(function (data, status, config, header) {
                if (data) {
                    console.log("Rate", data);

                    $scope.rates = data;

                    $scope.numberOfPages = function () {
                        if ($scope.rates) {
                            return Math.ceil($scope.rates.length / $scope.pageSize);
                        }

                        return 1;
                    }
                }
            })
            .error(function (data, status, config, header) {
            }
        );
    };

    $scope.chooseShippingService = function (rate) {
        $scope.shipVendor = rate.carrier;
        $scope.shipVia = rate.shipVia;
        $scope.shipMethodCode = rate.code;
    };

    if ($scope.req.items.length > 0) {
        var contentWeight = 0;
        for (var i = 0; i < $scope.req.items.length; i++) {
            var item = $scope.req.items[i];
            contentWeight += item.totalItemWeight;
        }
        $scope.contentWeight = contentWeight;
    }
});

posControllers.controller('OrderPendingCtrl', function ($rootScope, $scope, $location, $http, $routeParams, ngDialog) {
    $scope.currentPage = 0;
    $scope.pageSize = 8;
    $scope.numberOfPages = 0;

    $http.get(getURL($rootScope) + "/flow/POS.Sales.GetRecentOrdersByStatus", {params: {'associateId': $rootScope.user.id, 'status': 'Draft', 'storeName': $rootScope.user.storeName}})
        .success(function (data, status, headers, config) {
            if (data) {
                $scope.orders = data;

                $scope.currentPage = 0;
                $scope.numberOfPages = function () {
                    if ($scope.orders) {
                        return Math.ceil($scope.orders.length / $scope.pageSize);
                    }

                    return 1;
                }
            }
        })
        .error(function (data, status, headers, config) {
        }
    );

    $scope.continueTrx = function (order) {
        $location.path('/checkout').search('orderId', order.id);
    };

    $scope.removePendingOrder = function (order) {
        $http.delete(getURL($rootScope) + '/flow/POS.Sales.DeleteSales', {params: {'orderId': order.id}})
            .success(function (data, status, headers, config) {
                var index = $scope.orders.indexOf(order);
                if (index != -1) {
                    $scope.orders.splice(index, 1);
                }
            })
            .error(function (data, status, headers, config) {
                $scope.popupMsg = data;
                ngDialog.open({scope: $scope});
            }
        );
    };
});

posControllers.controller('OrderRecentCtrl', function ($rootScope, $scope, $location, $http, $routeParams, ngDialog) {
    $scope.currentPage = 0;
    $scope.pageSize = 8;
    $scope.numberOfPages = 0;

    $http.get(getURL($rootScope) + "/flow/POS.Sales.GetRecentOrders", {params: {'storeName': $rootScope.user.storeName, 'days': 3}})
        .success(function (data, status, headers, config) {
            if (data) {
                $scope.orders = data;

                $scope.currentPage = 0;
                $scope.numberOfPages = function () {
                    if ($scope.orders) {
                        return Math.ceil($scope.orders.length / $scope.pageSize);
                    }

                    return 1;
                }
            }
        })
        .error(function (data, status, headers, config) {
        }
    );

    $scope.displayOrderDetail = function (orderId) {
        var queryParams = {
            'orderId': orderId,
            'phoneNumber': $scope.phoneNumber,
            'orderNumber': $scope.orderNumber
        };

        var queryString = qs(queryParams);
        console.log("Query", queryString);

        $location.path('/orderdetail').search(queryString);
    };
});

posControllers.controller('OrderDefaultCtrl', function ($rootScope, $scope, $location, $http, $routeParams, ngDialog) {
    $scope.currentPage = 0;
    $scope.pageSize = 6;
    $scope.numberOfPages = 0;

    var customerId = $routeParams.customerId;
    if (customerId && Number(customerId) > 0) {
        $http.get(getURL($rootScope) + "/flow/POS.Customer.LookupBy", {params: {'by': 'id', 'value': customerId}})
            .success(function (data, status, headers, config) {
                if (data) {
                    $scope.customer = data;
                    $scope.phoneNumber = $scope.customer.phoneNumber;

                    $http.get(getURL($rootScope) + "/flow/POS.Customer.GetCustomerOrders", {params: {'customerId': $scope.customer.id}})
                        .success(function (data, status, headers, config) {
                            if (data) {
                                $scope.orders = data;

                                $scope.currentPage = 0;
                                $scope.numberOfPages = function () {
                                    if ($scope.orders) {
                                        return Math.ceil($scope.orders.length / $scope.pageSize);
                                    }

                                    return 1;
                                }
                            }
                        })
                        .error(function (data, status, headers, config) {
                            $scope.popupMsg = "No previous orders";
                            ngDialog.open({scope: $scope});
                        }
                    );
                }
            })
            .error(function (data, status, headers, config) {
                $scope.popupMsg = data;
                ngDialog.open({scope: $scope});
            }
        );
    }

    $scope.searchOrders = function () {
        if ($scope.orderNumber && $scope.orderNumber.length > 0) {
            $scope.lookupOrder($scope.orderNumber);
        }
        else if ($scope.itemNumber && $scope.itemNumber.length > 0) {
            $scope.lookupByItemSKU($scope.itemNumber);
        }
        if ($scope.phoneNumber && $scope.phoneNumber.length > 0) {
            $scope.lookupCustomer($scope.phoneNumber);
        }
    };

    $scope.lookupCustomer = function (phoneNumber) {
        $http.get(getURL($rootScope) + "/flow/POS.Customer.LookupBy", {params: {'by': 'phone', 'value': phoneNumber}})
            .success(function (data, status, headers, config) {
                if (data) {
                    $scope.customer = data;
                    $http.get(getURL($rootScope) + "/flow/POS.Customer.GetCustomerOrders", {params: {'customerId': $scope.customer.id}})
                        .success(function (data, status, headers, config) {
                            if (data) {
                                $scope.orders = data;

                                $scope.currentPage = 0;
                                $scope.numberOfPages = function () {
                                    if ($scope.orders) {
                                        return Math.ceil($scope.orders.length / $scope.pageSize);
                                    }

                                    return 1;
                                }
                            }
                        })
                        .error(function (data, status, headers, config) {
                            $scope.popupMsg = "No previous orders";
                            ngDialog.open({scope: $scope});
                        }
                    );
                }
            })
            .error(function (data, status, headers, config) {
                $scope.popupMsg = "No customers found [" + phoneNumber + "].";
                ngDialog.open({scope: $scope});

                $scope.orders = [];
            }
        );
    };

    $scope.lookupOrder = function (orderNumber) {
        $http.get(getURL($rootScope) + "/flow/POS.Sales.GetSalesSummaryByNumber", {params: {'orderId': orderNumber}})
            .success(function (data, status, headers, config) {
                console.log("Data", data);

                if (data) {
                    $scope.currentPage = 0;
                    $scope.numberOfPages = 1;
                    $scope.orders = [];
                    $scope.orders.push(data);
                    $scope.orderNumber = orderNumber;
                }
            })
            .error(function (data, status, headers, config) {
                $scope.popupMsg = 'No order found [' + orderNumber + '].';
                ngDialog.open({scope: $scope});
            }
        );
    };

    $scope.lookupByItemSKU = function (itemNumber) {
        if (!itemNumber) {
            $scope.popupMsg = 'Please enter Item SKU';
            ngDialog.open({scope: $scope});

            return;
        }

        $http.get(getURL($rootScope) + "/flow/POS.Sales.GetSalesSummaryByItemNumber", {params: {'itemNumber': itemNumber}})
            .success(function (data, status, headers, config) {
                console.log("Data", data);

                if (data) {
                    $scope.currentPage = 0;
                    $scope.numberOfPages = 1;
                    $scope.orders = data;
                    $scope.numberOfPages = function () {
                        if ($scope.orders) {
                            return Math.ceil($scope.orders.length / $scope.pageSize);
                        }

                        return 1;
                    }
                }
            })
            .error(function (data, status, headers, config) {
                $scope.popupMsg = 'No order found [' + orderNumber + '].';
                ngDialog.open({scope: $scope});
            }
        );
    };

    $scope.lookupByDate = function (startDate, endDate) {
        if (!startDate) {
            $scope.popupMsg = 'Please Start Date';
            ngDialog.open({scope: $scope});
            return;
        }
        if (!endDate) {
            $scope.popupMsg = 'Please End Date';
            ngDialog.open({scope: $scope});
            return;
        }
        $http.get(getURL($rootScope) + "/flow/POS.Sales.GetSalesSummaryByDate", {params: {'startDate': startDate, 'endDate': endDate}})
            .success(function (data, status, headers, config) {
                if (data) {
                    $scope.numberOfPages = 1;
                    $scope.orders = data;
                    $scope.currentPage = 0;
                    $scope.numberOfPages = function () {
                        if ($scope.orders) {
                            return Math.ceil($scope.orders.length / $scope.pageSize);
                        }

                        return 1;
                    }
                }
            })
            .error(function (data, status, headers, config) {
                $scope.popupMsg = 'No order found [' + orderNumber + '].';
                ngDialog.open({scope: $scope});
            }
        );
    };

    $scope.displayOrderDetail = function (orderId) {
        var queryParams = {
            'phoneNumber': $scope.phoneNumber,
            'orderId': orderId
        };

        var queryString = qs(queryParams);
        console.log("Query", queryString);

        $location.path('/orderdetail').search(queryString);
    };

    $scope.viewCustomerDetail = function (customerId) {
        if (customerId > 0) {
            /* Converts an object into a key/value par with an optional prefix. Used for converting objects to a query string */
            var searchParams = {
                'cid': customerId,
                'phoneNumber': $scope.phoneNumber,
                'email': $scope.email,
                'customerName': $scope.customerName,
                'dob': $scope.dob,
                'zipCode': $scope.zipCode,
                'driverLicense': $scope.driverLicense,
                'fromPage': "checkout"
            };

            var queryString = qs(searchParams);
            $location.path('/customer_detail').search(queryString);
        }
    };

    var phoneN = $location.search().phoneNumber;
    if (phoneN) {
        console.log("Search by phone");
        $scope.lookupCustomer(phoneN);
        $scope.phoneNumber = phoneN;
    }
    var orderNumber = $location.search().orderNumber;
    if (orderNumber) {
        console.log("Search by order");
        $scope.lookupOrder(orderNumber);
    }

    $location.$$search = {};
});

posControllers.controller('OrderDetailCtrl', function ($rootScope, $scope, $location, $http, $routeParams, ngDialog) {
    $scope.panes = [
        {
            "name": "Order Details",
            "template": "templates/orderdetail-header.html",
            "controller": "OrderDetailHeaderCtrl",
            "includedInTabView": true
        },
        {
            "name": "Credit / Refund",
            "template": "templates/orderdetail-returnables.html",
            "controller": "OrderDetailReturnableCtrl",
            "includedInTabView": true
        },
        {
            "name": "Returned Items",
            "template": "templates/orderdetail-returned.html",
            "controller": "OrderDetailReturnedCtrl",
            "includedInTabView": true
        },
        {
            "name": "Payments",
            "template": "templates/orderdetail-payments.html",
            "controller": "OrderDetailPaymentsCtrl",
            "includedInTabView": true
        }
    ];
});

posControllers.controller('OrderDetailPaymentsCtrl', function ($rootScope, $scope, $location, $http, $routeParams, ngDialog) {
    $scope.queryParams = $location.search();
    $scope.orderId = $routeParams.orderId;

    $location.$$search = {};

    if ($scope.orderId > 0) {
        $http.get(getURL($rootScope) + "/flow/POS.Sales.GetPayments", {'params': {'orderId': $scope.orderId}})
            .success(function (data, status, headers, config) {
                if (data) {
                    $scope.payments = data;
                }
            })
            .error(function (data, status, headers, config) {
            }
        );
    }

    $scope.goBackToOrders = function () {
        var queryString = qs($scope.queryParams);
        $location.path('/orders').search(queryString);
    };

    $scope.showSignature = function (payment) {
        if (payment) {
            if (payment.signature) {
                if (payment.signature.indexOf(",") != -1) {
                    //var temp = payment.signature;
                    //temp = temp.replace("///^/g", " ");
                    //console.log("Vector: ", temp);

                    $scope.signatureData = payment.signature;
                    $scope.productImage = "";
                }
                else {
                    $scope.productImage = 'data:image/png;base64,' + payment.signature;
                }
            }
            else {
                $scope.productImage = "";
            }
            $scope.authCode = payment.authCode;
            $scope.orderId = payment.orderId;
            $scope.historyId = payment.historyId;

            ngDialog.open({template: 'templates/display-signature.html', scope: $scope, className: 'ngdialog-theme-mine'});
        }
    };
});

posControllers.controller('OrderDetailReturnedCtrl', function ($cookieStore, $rootScope, $scope, $location, $http, $routeParams, ngDialog) {
    $scope.queryParams = $location.search();
    $scope.orderId = $routeParams.orderId;

    $location.$$search = {};

    if ($scope.orderId > 0) {
        $http.get(getURL($rootScope) + "/flow/POS.Sales.GetReturned", {'params': {'orderId': $scope.orderId}})
            .success(function (data, status, headers, config) {
                if (data) {
                    $scope.order = data;
                    $scope.eligibleForReturn = data.eligibleForReturn;
                }
            })
            .error(function (data, status, headers, config) {

            }
        );
    }

    $scope.goBackToOrders = function () {
        var queryString = qs($scope.queryParams);
        $location.path('/orders').search(queryString);
    };

    $scope.printReceipt = function (receiptType, transactionNumber) {
        var sessionKey = $cookieStore.get('sessionKey');
        var receiptFont = $rootScope.storeProfile.receiptFont;
        printReceipt(getURL($rootScope), sessionKey, $scope.orderId, false, true, receiptType, transactionNumber, false, receiptFont);
    };

});

posControllers.controller('OrderDetailReturnableCtrl', function ($cookieStore, $rootScope, $scope, $location, $http, $routeParams, ngDialog) {
    $scope.queryParams = $location.search();
    $scope.orderId = $routeParams.orderId;
    $scope.emailReceiptFlag = $rootScope.storeProfile.emailReceipt;
    $scope.printReceiptFlag = $rootScope.storeProfile.printReceipt;
    $scope.eligibleForReturn = true;
    $scope.eligibleForRefund = $rootScope.storeProfile.allowRefund;

    $location.$$search = {};

    if ($scope.orderId > 0) {
        $http.get(getURL($rootScope) + "/flow/POS.Sales.GetReturnables", {'params': {'orderId': $scope.orderId,'storeId': $rootScope.user.storeId}})
            .success(function (data, status, headers, config) {
                if (data) {
                    $scope.order = data;
                    $scope.eligibleForReturn = (data.eligibleForReturn == 'YES');
                    if($scope.eligibleForRefund) {
                        $scope.eligibleForRefund = (data.eligibleForRefund == 'YES');
                    }
                }
            })
            .error(function (data, status, headers, config) {
            }
        );
    }

    function returnSalesLine($scope, gotoCheckout) {
        if ($scope.order.lines && $scope.order.lines.length > 0) {
            var returnLines = [];
            var tmpLines = [];

            for (var i = 0; i < $scope.order.lines.length; i++) {
                var line = $scope.order.lines[i];
                if (line.returnQty > 0) {
                    tmpLines.push(line);

                    returnLines.push({
                        "companyId": $rootScope.user.companyId,
                        "customerId": $scope.order.customer.id,
                        "lineId": line.id,
                        "storeId": $rootScope.user.storeId,
                        "storeName": $rootScope.user.storeName,
                        "salesId": $scope.orderId,
                        "itemId": line.itemId,
                        "returnQty": line.returnQty,
                        "returnTotal": line.returnTotal,
                        "associateId": $rootScope.user.id,
                        "note": line.note
                    });
                }
            }

            $http.post(getURL($rootScope) + "/flow/POS.Sales.PostCustomerReturn", returnLines)
                .success(function (data, status, headers, config) {
                    if (data) {
                        $scope.message = "Returned";

                        for (var i = 0; i < tmpLines.length; i++) {
                            var line = tmpLines[i];

                            var index = $scope.order.lines.indexOf(line);
                            if (index != -1) {
                                $scope.order.lines.splice(index, 1);
                            }
                        }

                        tmpLines = [];

                        $scope.eligibleForRefund = false;
                        var sessionKey = $cookieStore.get('sessionKey');
                        var receiptFont = $rootScope.storeProfile.receiptFont;
                        printReceipt(getURL($rootScope), sessionKey, $scope.orderId, true, $scope.printReceiptFlag, 'return', data, false, receiptFont);
                        if ($scope.emailReceiptFlag) {
                            $scope.emailReceipt(data);
                        }

                        if (gotoCheckout) {
                            $http.get(getURL($rootScope) + "/flow/POS.Customer.LookupBy", {params: {'by': 'id', 'value': $scope.order.customer.id}})
                                .success(function (data, status, headers, config) {
                                    if (data) {
                                        $rootScope.currentCheckout.customer = data;
                                        $location.path('/checkout');
                                    }
                                }
                            );
                        }
                    }
                })
                .error(function (data, status, headers, config) {
                    $scope.popupMsg = "Error: " + data + ". Please try again";
                    ngDialog.open({scope: $scope});
                }
            );
        }
    }

    $scope.emailReceipt = function (txNumber) {
        if ($scope.orderId) {
            var url = getURL($rootScope) + "/flow/POS.Transact.EmailReceipt";
            $http.get(url, {params: {'saleId': $scope.orderId, 'receiptType': 'return', 'transactionNumber': txNumber}}).
                success(function (data, status, headers, config) {
                }).
                error(function (data, status, headers, config) {
                    $scope.popupMsg = data;
                    ngDialog.open({scope: $scope});
                }
            );
        }
    };

    $scope.goBackToOrders = function () {
        var queryString = qs($scope.queryParams);
        $location.path('/orders').search(queryString);
    };

    $scope.decrementQuantity = function (line) {
        if (line.returnQty == 0) {
            $scope.popupMsg = "Invalid request";
            ngDialog.open({scope: $scope});
        }
        else {
            line.returnQty = line.returnQty - 1;
            line.returnTotal = round(line.lineSubtotalDue / line.quantity * line.returnQty, 2);
        }
    };

    $scope.incrementQuantity = function (line) {
        if (line.quantity - 1 >= line.returnQty) {
            line.returnQty = line.returnQty + 1;
            line.returnTotal = round(line.lineSubtotalDue / line.quantity * line.returnQty, 2);
        }
        else {
            $scope.popupMsg = "You can't return more than what was bought [" + line.quantity + "].";
            ngDialog.open({scope: $scope});
        }
    };

    $scope.checkReturn = function ($event, line) {
        var checkbox = $event.target;
        if (checkbox.checked && line.returnQty == 0) {
            line.returnQty = line.returnQty + 1;
        }
        else if (!checkbox.checked && line.returnQty == 1) {
            line.returnQty = line.returnQty - 1;
        }

        line.returnTotal = round(line.lineSubtotalDue / line.quantity * line.returnQty, 2);
    };

    $scope.commitSalesLinesReturn = function (gotoCheckout) {
        if ($scope.order.customer == null || $scope.order.customer.id < 0) {
            $scope.popupMsg = "You must look up the customer. If the customer doesn't exist, please create before returning product.";
            ngDialog.open({scope: $scope});

            return;
        }
        else {
            var hasSelected = false;
            if ($scope.order.lines && $scope.order.lines.length > 0) {
                var returnLines = [];

                for (var i = 0; i < $scope.order.lines.length; i++) {
                    var line = $scope.order.lines[i];
                    if (line.returnQty > 0) {
                        hasSelected = true;
                        break;
                    }
                }
            }

            if (!hasSelected) {
                $scope.popupMsg = "Please select the item to return";
                ngDialog.open({scope: $scope});


                return;
            }

            if (!$rootScope.user.manager) {
                var dcManagerOverride = ngDialog.open({template: 'templates/dc-manager-override.html', scope: $scope, controller: 'DCManagerOverrideCtrl'});

                dcManagerOverride.closePromise.then(function (data) {
                    var returnValue = data.value;

                    if (returnValue == 1) {
                        returnSalesLine($scope, gotoCheckout);
                    }
                });
            }
            else {
                returnSalesLine($scope, gotoCheckout);
            }
        }
    };

    $scope.commitSalesLinesRefund = function () {
        if ($scope.order.customer == null || $scope.order.customer.id < 0) {
            $scope.popupMsg = "You must look up the customer. If the customer doesn't exist, please create before returning product.";
            ngDialog.open({scope: $scope});

            return;
        }
        else {
            var hasSelected = false;
            if ($scope.order.lines && $scope.order.lines.length > 0) {
                var returnLines = [];

                for (var i = 0; i < $scope.order.lines.length; i++) {
                    var line = $scope.order.lines[i];
                    if (line.returnQty > 0) {
                        hasSelected = true;
                        break;
                    }
                }
            }

            if (!hasSelected) {
                $scope.popupMsg = "Please select the item to refund";
                ngDialog.open({scope: $scope});


                return;
            }

            if (!$rootScope.user.manager) {
                var dcManagerOverride = ngDialog.open({template: 'templates/dc-manager-override.html', scope: $scope, controller: 'DCManagerOverrideCtrl'});

                dcManagerOverride.closePromise.then(function (data) {
                    var returnValue = data.value;

                    if (returnValue == 1) {
                        refundSalesLine($scope);
                    }
                });
            }
            else {
                refundSalesLine($scope);
            }
        }
    };

    function refundSalesLine($scope) {
        if ($scope.order.lines && $scope.order.lines.length > 0) {
            var refundLines = [];
            var tmpLines = [];

            for (var i = 0; i < $scope.order.lines.length; i++) {
                var line = $scope.order.lines[i];
                if (line.returnQty > 0) {
                    tmpLines.push(line);

                    refundLines.push({
                        "companyId": $rootScope.user.companyId,
                        "customerId": $scope.order.customer.id,
                        "lineId": line.id,
                        "storeId": $rootScope.user.storeId,
                        "storeName": $rootScope.user.storeName,
                        "salesId": $scope.orderId,
                        "itemId": line.itemId,
                        "returnQty": line.returnQty,
                        "returnTotal": line.returnTotal,
                        "associateId": $rootScope.user.id,
                        "note": line.note
                    });
                }
            }

            $http.post(getURL($rootScope) + "/flow/POS.Sales.GetRefundables", refundLines)
                .success(function (data, status, headers, config) {
                    if (data) {
                        $scope.paymentRefund = data;
                        var refundables = $scope.paymentRefund.refundables;
                        if(refundables && refundables.length > 0) {
                            for (var i = 0; i < refundables.length; i++) {
                                if('CS' == refundables[i].type) {
                                    $scope.cashRefundAmount = round(refundables[i].amount,2);
                                }
                                else if('SC' == refundables[i].type) {
                                    $scope.storeCreditRefundAmount = round(refundables[i].amount,2);
                                }
                                else if('CC' == refundables[i].type) {
                                    $scope.creditCardRefundAmount = round(refundables[i].amount,2);
                                }
                            }
                        }

                        if ($scope.paymentRefund.refundDue > 0) {
                            $scope.refundLines = refundLines;
                            $scope.salesId = $scope.orderId;
                            if ($scope.order.customer && $scope.order.customer.storeCredit > 0) {
                                $scope.customerId = $scope.order.customer.id;
                                $scope.storeCredit = $scope.order.customer.storeCredit;
                            }

                            var checkoutDialog = ngDialog.open({template: 'templates/refund.html', scope: $scope, controller: 'RefundCtrl', className: 'ngdialog-theme-mine'});
                            checkoutDialog.closePromise.then(function (data) {
                                $http.get(getURL($rootScope) + "/flow/POS.Sales.GetReturnables", {'params': {'orderId': $scope.orderId,'storeId': $rootScope.user.storeId}})
                                    .success(function (data, status, headers, config) {
                                        if (data) {
                                            $scope.order = data;
                                            $scope.eligibleForReturn = (data.eligibleForReturn == 'YES');
                                        }
                                    })
                                    .error(function (data, status, headers, config) {
                                    }
                                );
                            });
                        }
                    }
                })
                .error(function (data, status, headers, config) {
                    $scope.popupMsg = "Error: " + data + ". Please try again";
                    ngDialog.open({scope: $scope});
                }
            );
        }
    }
});


posControllers.controller('RefundCtrl', function ($rootScope, $scope, $http, $cookieStore, ngDialog, $q, $window, $timeout) {
    $scope.payAmount = "";
    $scope.paymentType = "";
    $scope.ccManualEntry = false;
    $scope.paymentId = -1;
    $scope.showDebitPinInput = false;
    $scope.emailReceiptFlag = $rootScope.storeProfile.emailReceipt;
    $scope.printReceiptFlag = $rootScope.storeProfile.printReceipt;
    $scope.btn_refund_clicked = false;
    $scope.btn_done_clicked = false;
    $scope.refundDue = round($scope.paymentRefund.refundDue,2);

    $scope.showOpenCashRegister = false;

    $scope.clear = function () {
        var canvas = document.getElementById('canvas');
        var context = canvas.getContext('2d');
        context.clearRect(0, 0, canvas.width, canvas.height)
    };

    $scope.openCashRegister = function () {
        openCashRegister();
    };

    $scope.emailReceipt = function () {
        if ($scope.salesId) {
            var url = getURL($rootScope) + "/flow/POS.Transact.EmailReceipt";
            $http.get(url, {params: {'saleId': $scope.salesId, 'customerEmail': $scope.customerEmail}}).
                success(function (data, status, headers, config) {
                }).
                error(function (data, status, headers, config) {
                    $scope.popupMsg = data;
                    ngDialog.open({scope: $scope});
                }
            );
        }
    };

    $scope.processRefund = function () {
        $scope.btn_refund_clicked = true;

        var refundRequest = {
            "user": $rootScope.user,
            "amount": $scope.creditCardRefundAmount,
            "customerId": $scope.customerId,
            "salesId": $scope.salesId,
            "refundLines" : $scope.refundLines,
            "refundablePayments" : $scope.paymentRefund.refundables
        };

        var url = getURL($rootScope) + "/flow/POS.Sales.PostRefund";

        $http.post(url, refundRequest)
            .success(function (data, status, headers, config) {
                if (data) {
                    $scope.paymentProcessingStatus = "Refunded Successfully";

                    var sessionKey = $cookieStore.get('sessionKey');
                    var receiptFont = $rootScope.storeProfile.receiptFont;
                    printReceipt(getURL($rootScope), sessionKey, $scope.salesId, true, $scope.printReceiptFlag, 'refund', data, true, receiptFont);
                    if ($scope.emailReceiptFlag) {
                        $scope.emailReceipt(data);
                    }
                }
            })
            .error(function (data, status, headers, config) {
                $scope.popupMsg = "Error: " + data + ". Please try again";
                ngDialog.open({scope: $scope});
            });
    };

    $scope.smartClose = function () {
        $scope.closeThisDialog(1);
    };

});

posControllers.controller('OrderDetailHeaderCtrl', function ($rootScope, $scope, $cookieStore, $location, $http, $routeParams, ngDialog) {
    $scope.customerWasLookedUp = false;
    $scope.queryParams = $location.search();
    $scope.isRecentOrder = false;

    $scope.btnCancelClicked = false;

    var printReceiptFlag = $rootScope.storeProfile.printReceipt;
    var orderId = $routeParams.orderId;
    $location.$$search = {};

    if (orderId > 0) {
        $http.get(getURL($rootScope) + "/flow/POS.Sales.GetSalesDetail", {'params': {'orderId': orderId}})
            .success(function (data, status, headers, config) {
                if (data) {
                    $scope.order = data;
                    $scope.eligibleForReturn = data.eligibleForReturn;

                    var today = new Date();
                    var orderDate = new Date(data.header.timestamp);

                    if (today.getYear() == orderDate.getYear() && today.getMonth() == orderDate.getMonth() && today.getDate() == orderDate.getDate() && (data.header.status == 'Complete' || data.header.status == 'FULL_PAYMENT')) { // within 1 hour
                        $scope.isRecentOrder = true;
                    }
                    else {
                        $scope.isRecentOrder = false;
                    }
                }
            })
            .error(function (data, status, headers, config) {
            }
        );
    }

    $scope.undoTransaction = function () {
        $scope.btnCancelClicked = true;

        $scope.dialogMsg = "Are you sure??";
        var checkoutDialog = ngDialog.open({template: 'templates/confirm-dialog.html', scope: $scope, controller: 'ConfirmDialogCtrl'});
        checkoutDialog.closePromise.then(function (data) {
            $scope.btnCancelClicked = false;

            var returnValue = data.value;

            if (returnValue == 1) {
                var rq = {
                    "salesId": $scope.order.header.id,
                    "userId": $rootScope.user.id,
                    "companyId": $rootScope.user.companyId,
                    "storeId": $rootScope.user.storeId,
                    "storeName": $rootScope.user.storeName
                };
                $http.post(getURL($rootScope) + "/flow/POS.Sales.UndoTransaction", rq)
                    .success(function (data, status, headers, config) {
                        $scope.message = "Transaction is reverted";
                        $scope.order = data;
                        $scope.eligibleForReturn = data.eligibleForReturn;
                        $scope.isRecentOrder = false;
                        var sessionKey = $cookieStore.get('sessionKey');
                        var receiptFont = $rootScope.storeProfile.receiptFont;
                        printReceipt(getURL($rootScope), sessionKey, $scope.order.header.id, false, true, 'voided', null, true, receiptFont);
                        if ($rootScope.storeProfile.emailReceipt) {
                            $scope.emailReceipt("voided");
                        }
                    })
                    .error(function (data, status, headers, config) {
                        $scope.popupMsg = data;
                        ngDialog.open({scope: $scope});
                    }
                );
            }
        });
    };

    $scope.goBackToOrders = function () {
        var queryString = qs($scope.queryParams);
        console.log("Query", queryString);
        $location.path('/orders').search(queryString);
    };

    $scope.printReceipt = function (receiptType) {
        var sessionKey = $cookieStore.get('sessionKey');
        var receiptFont = $rootScope.storeProfile.receiptFont;
        printReceipt(getURL($rootScope), sessionKey, orderId, false, true, receiptType, null, false, receiptFont);
    };

    $scope.emailReceipt = function (receiptType) {
        if ($scope.order.header.id) {
            var url = getURL($rootScope) + "/flow/POS.Transact.EmailReceipt";
            $http.get(url, {params: {'saleId': $scope.order.header.id, 'receiptType': receiptType}}).
                success(function (data, status, headers, config) {
                }).
                error(function (data, status, headers, config) {
                    $scope.popupMsg = data;
                    ngDialog.open({scope: $scope});
                }
            );
        }
    };

    $scope.lookupCustomer = function (phoneNumber) {
        if (phoneNumber) {
            phoneNumber = phoneNumber.replace(/-/g, "");

            var url = getURL($rootScope) + "/flow/POS.Customer.LookupBy";

            $http.get(url, {params: {'by': 'phone', 'value': phoneNumber}})
                .success(function (data, status, headers, config) {
                    if (data) {
                        $scope.order.customer = data;
                        $scope.customerWasLookedUp = true;
                    }
                })
                .error(function (data, status, headers, config) {
                    $scope.popupMsg = "No customer found";
                    ngDialog.open({scope: $scope});
                }
            );

        }
    };

    $scope.continueTrx = function (order) {
        $location.path('/checkout').search('orderId', order.id);
    };

    $scope.cancelSaleAsStoreCredit = function () {
        if (!$scope.order.customer) {
            $scope.popupMsg = "To cancel, the customer must be assigned. Please create or lookup the customer.";
            ngDialog.open({scope: $scope});

            return;
        }

        $scope.dialogMsg = "Are you sure??";
        var checkoutDialog = ngDialog.open({template: 'templates/confirm-dialog.html', scope: $scope, controller: 'ConfirmDialogCtrl'});
        checkoutDialog.closePromise.then(function (data) {
            //$scope.btnCancelClicked = false;

            var returnValue = data.value;

            if (returnValue == 1) {
                var rq = {
                    "cancelType": "ALL",
                    "salesId": $scope.order.header.id,
                    "returnMethod": "StoreCredit",
                    "totalCreditAmount": $scope.order.header.paymentsMade,
                    "userId": $rootScope.user.id,
                    "companyId": $rootScope.user.companyId,
                    "storeId": $rootScope.user.storeId,
                    "storeName": $rootScope.user.storeName,
                    "customerId": $scope.order.customer.id
                };
                $http.post(getURL($rootScope) + "/flow/POS.Sales.CancelSales", rq)
                    .success(function (data, status, headers, config) {
                        $scope.message = "Cancelled";
                        $scope.order = data;
                        $scope.eligibleForReturn = data.eligibleForReturn;
                        var sessionKey = $cookieStore.get('sessionKey');
                        var receiptFont = $rootScope.storeProfile.receiptFont;
                        printReceipt(getURL($rootScope), sessionKey, $scope.order.header.id, false, true, 'cancel', null, false, receiptFont);
                        if ($rootScope.storeProfile.emailReceipt) {
                            $scope.emailReceipt("cancel");
                        }
                    })
                    .error(function (data, status, headers, config) {
                    }
                );
            }
        });
    };

    $scope.payRemainingOnSale = function () {
        $scope.total = $scope.order.header.total;
        $scope.paymentDue = $scope.order.header.total - $scope.order.header.paymentsMade;
        if ($scope.paymentDue > 0) {
            $scope.salesId = $scope.order.header.id;
            if ($scope.order.customer && $scope.order.customer.storeCredit > 0) {
                $scope.customerId = $scope.order.customer.id;
                $scope.storeCredit = $scope.order.customer.storeCredit;
            }

            var checkoutDialog = ngDialog.open({template: 'templates/checkout.html', scope: $scope, controller: 'CheckoutCtrl', className: 'ngdialog-theme-mine'});
            checkoutDialog.closePromise.then(function (data) {
                $http.get(getURL($rootScope) + "/flow/POS.Sales.GetSalesDetail", {'params': {'orderId': $scope.order.header.id}})
                    .success(function (data, status, headers, config) {
                        if (data) {
                            $scope.order = data;
                            $scope.eligibleForReturn = data.eligibleForReturn;
                        }
                    })
                    .error(function (data, status, headers, config) {
                    }
                );
            });
        }
    };

    $scope.attachOrderToCustomer = function (customerId, salesId) {
        $http.post(getURL($rootScope) + '/flow/POS.Sales.AttachCustomer', {'customerId': customerId, 'salesId': salesId})
            .success(function (data, status, headers, config) {
                $scope.customerWasLookedUp = false;
            })
            .error(function (data, status, headers, config) {
                $scope.popupMsg = "Error while attaching the Order to Customer";
                ngDialog.open({scope: $scope});
            }
        );
    };

    $scope.confirmCustomerPickup = function (line) {
        var req = {
            'lineId': line.id,
            'itemNumber': line.sku,
            'salesId': $scope.order.header.id,
            'store': $rootScope.user.storeName,
            'userId': $rootScope.user.id
        };
        $http.post(getURL($rootScope) + '/flow/POS.Sales.ConfirmCustomerPickup', req)
            .success(function (data, status, headers, config) {
                $scope.order = data;
                $scope.eligibleForReturn = data.eligibleForReturn;

                // TODO PRINT THE CUSTOMER PICK UP RECEIPT
            })
            .error(function (data, status, headers, config) {
                $scope.popupMsg = "Error while confirming the customer pickup.";
                ngDialog.open({scope: $scope});
            }
        );
    };
});