'use strict';

var posControllers = angular.module('posGlobalControllers', []);

function isBlank(str) {
    return (!str || /^\s*$/.test(str));
}

function getURL(rootScope) {
    return rootScope.apiEndPoint;
}

function qs(obj, prefix) {
    var str = [];
    for (var p in obj) {
        var k = prefix ? prefix + "[" + p + "]" : p,
            v = obj[k];
        if (k && v) {
            str.push(angular.isObject(v) ? qs(v, k) : (k) + "=" + encodeURIComponent(v));
        }
    }
    return str.join("&");
}

function round(value, exp) {
    if (typeof exp === 'undefined' || +exp === 0) {
        return Math.round(value);
    }

    value = +value;
    exp = +exp;

    if (isNaN(value) || !(typeof exp === 'number' && exp % 1 === 0)) {
        return NaN;
    }

    // Shift
    value = value.toString().split('e');
    value = Math.round(+(value[0] + 'e' + (value[1] ? (+value[1] + exp) : exp)));

    // Shift back
    value = value.toString().split('e');
    return +(value[0] + 'e' + (value[1] ? (+value[1] - exp) : -exp));
}

function initializeCurrentCheckout(rootScope) {
    if (rootScope.currentCheckout == null) {
        rootScope.currentCheckout = {
            "items": [],
            "customer": {},
            "payments": [],
            "discountRate": 0
        };
    }
}

function callExternalURL(url, callBackFunction) {
    if (window.cordova !== undefined) {
        cordova.exec(
            function callback(data) {
                callBackFunction(data);
            },
            // Register the errorHandler
            function errorHandler(err) {
                alert("Error calling "+url+": " + err);
            },
            'POSUtil',
            'callExternalURL',
            [url, 300]
        );
    }
}

function callExternalURLWithErrorCallBack(url, timeOut, callBackFunction, errorCallBackFunction) {
    if (window.cordova !== undefined) {
        cordova.exec(
            function callback(data) {
                callBackFunction(data);
            },
            // Register the errorHandler
            function errorHandler(err) {
                errorCallBackFunction(err);
            },
            'POSUtil',
            'callExternalURL',
            [url, timeOut]
        );
    }
}

function getMacAddress() {
    if (window.cordova !== undefined) {
        cordova.exec(
            function callback(data) {
                alert(data); // mac address
            },
            // Register the errorHandler
            function errorHandler(err) {
                alert("error while getting MAC address")
            },
            'POSUtil',
            'getMacAddress',
            []
        );
    }
}

function callAlert(result) {
    alert(result);
}

function printReceipt(serverURL, sessionKey, saleId, printMerchantCopy, printCustomerCopy, receiptType, transactionNumber, openRegister, receiptFont) {
    if (window.cordova !== undefined) {
        var printerIP = window.localStorage.getItem("printerIP");
        var merchantCopy = printMerchantCopy ? "true" : "false";
        var customerCopy = printCustomerCopy ? "true" : "false";

        var companyCode = sessionKey.substring(0, sessionKey.indexOf('|'));
        cordova.exec(
            function callback(data) {
                console.log('Succeeded : ' + data);
            },
            // Register the errorHandler
            function errorHandler(err) {
                alert("Error while printing : " + err);
            },
            'StarPrinter',
            'printSalesLabel',
            [companyCode, serverURL, sessionKey, saleId, printerIP, merchantCopy, customerCopy, receiptType, transactionNumber, openRegister, receiptFont]
        );
    }
}

function printLabel(serverURL, sessionKey, saleId, labelType, receipt) {
    if (window.cordova !== undefined) {
        var printerIP = window.localStorage.getItem("labelPrinterIP");
        cordova.exec(
            function callback(data) {
                console.log('Succeeded : ' + data);
            },
            // Register the errorHandler
            function errorHandler(err) {
                alert("Error while printing : " + err);
            },
            'ZebraPrinter',
            'printLabel',
            [serverURL, sessionKey, saleId, printerIP, '9100', labelType, receipt]
        );
    }
}

function openCashRegister() {
    if (window.cordova !== undefined) {
        var printerIP = window.localStorage.getItem("printerIP");
        cordova.exec(
            function callback(data) {
                console.log('Succeeded ');
            },
            // Register the errorHandlerf
            function errorHandler(err) {
                alert("Can't open the drawer");
            },
            'StarPrinter',
            'openCashRegister',
            [printerIP]
        );
    }
}

Number.prototype.formatMoney = function (c, d, t) {
    var n = this,
        c = isNaN(c = Math.abs(c)) ? 2 : c,
        d = d == undefined ? "." : d,
        t = t == undefined ? "," : t,
        s = n < 0 ? "-" : "",
        i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "",
        j = i.length;
    j = j > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
}

posControllers.controller('LoginCtrl', function ($rootScope, $scope, $route, $location, $http, $cookieStore, ngDialog, $templateCache) {
    var sessionKey = $location.search().sessionKey;
    var isUA = $location.search().isUA;
    var apiEndPoint = $location.search().apiEndPoint;

    if (sessionKey) {
        $cookieStore.put('sessionKey', sessionKey);
    }

    var storage = window.localStorage;
    if (isUA) {
        storage.setItem("isUA", "true");
    }
    if (apiEndPoint) {
        $rootScope.apiEndPoint = apiEndPoint;
    }

    $rootScope.needManagerApproval = false;
    $http.get(getURL($rootScope) + "/auth/post")
        .success(function (user, status, headers, config) {
            if (user) {
                // was successful
                $rootScope.user = user;
                $rootScope.loginUrl = user.loginUrl;

                storage.setItem("company", user.company);
                storage.setItem("store", user.storeName);
                storage.setItem("username", user.username);

                // Load Store Profile
                $http.get(getURL($rootScope) + "/flow/POS.Store.GetProfile", {params: {'storeId': user.storeId, 'userId': user.id}})
                    .success(function (storeProfile, status, headers, config) {
                        if (window.cordova !== undefined) {
                            cordova.exec(
                                function callback(data) {
                                    //callBackFunction(data);
                                },
                                // Register the errorHandler
                                function errorHandler(err) {
                                    //alert("Error calling : " + err);
                                },
                                'POSUtil',
                                'clearCache',
                                []
                            );
                        }

                        storeProfile.worldPay = {};
                        $rootScope.storeProfile = storeProfile;
                        $rootScope.currentCheckout = null;
                        $templateCache.removeAll();
                        $location.$$search = {};
                        $location.path('/checkout');
                    })
                    .error(function (data, status, headers, config) {
                        $scope.popupMsg = data;
                        ngDialog.open({scope: $scope});
                    }
                );
            }
            else {
                $scope.login.error = "Unable to login. Please check the data and try again";
            }
        })
        .error(function (data, status, headers, config) {
            $scope.popupMsg = data;
            ngDialog.open({scope: $scope});
        }
    );
});

posControllers.controller('ConfirmDialogCtrl', function ($scope) {
    $scope.confirm = function () {
        $scope.closeThisDialog(1);
    };
    $scope.decline = function () {
        $scope.closeThisDialog(0);
    }
});

posControllers.controller('DCManagerOverrideCtrl', function ($rootScope, $scope, $route, $location, $http) {
    $scope.pin = '';
    $scope.inputPinCode = function (num) {
        if (num == 'Back') {
            if ($scope.pin.length > 0) {
                $scope.pin = $scope.pin.substring(0, $scope.pin.length - 1);
            }
            return;
        }

        $scope.pin= $scope.pin.concat(num);
    };

    $scope.switchAssociate = function () {
        if (!$scope.pin) {
            $scope.error = "Please enter PIN #";

            return;
        }

        if ($scope.pin.length < 4) {
            $scope.error = "PIN # must be at least 4 characters";

            return;
        }

        var url = getURL($rootScope) + "/flow/POS.Associate.VerifyByPin";

        $http.get(url, {
            params: {'storeId': $rootScope.user.storeId, 'pin': $scope.pin, 'action': 'Manager DC Override'}
        }).success(function (data, status, headers, config) {
            if (data && data.id > 0) {
                $scope.closeThisDialog(1);
            }
            else {
                $scope.error = "Unauthorized";
                $scope.pin = '';
            }
        }).error(function (data, status, headers, config) {
                $scope.error = "Unauthorized";
                $scope.pin = '';
            }
        );
    };
});

posControllers.controller('TopBarCtrl', function ($rootScope, $scope, $location, $cookieStore, $http, $window, ngDialog) {
    var url = getURL($rootScope) + "/flow/POS.Associate.GetActiveAssociatesInStore";

    $scope.switchUser = function () {
        var checkoutDialog = ngDialog.open({template: 'templates/switch-associate.html', scope: $scope, controller: 'SwitchAssociateCtrl'});

        checkoutDialog.closePromise.then(function (data) {
            var returnValue = data.value;

            if (returnValue > 0) {
                $scope.paymentDue = 0;
                $scope.payAmount = "";
                $scope.preventEdit = true;
                $scope.paymentType = "";
                $scope.showDebitPinInput = false;
                $scope.transactData = {};
                $scope.ccManualEntry = false;
                $scope.salesId = "";

                $rootScope.currentCheckout = {
                    "items": [],
                    "customer": {},
                    "payments": [],
                    "discountRate": 0
                };

                $rootScope.needManagerApproval = false;
                $scope.subtotal = 0;
                $scope.tax = "";
                $scope.total = "";
                $scope.paymentProcessingStatus = "";

                $location.$$search = {};
                $location.path('/checkout');
            }
        })
    };

    $scope.goNext = function (url) {
        $location.path(url);
    };

    $scope.logout = function () {
        $cookieStore.remove('sessionKey');

        console.log("Login", $rootScope.loginUrl);

        if (window.cordova !== undefined && window.cordova.plugins.MagTek) {
            cordova.exec(
                function success(value) {
                    if (value) {
                        cordova.exec(
                            function success(value) {
                            },
                            function failed() {
                            }, 'com.egood.magtek-udynamo', 'closeDevice', []
                        );
                    }
                }, function failed() {
                }, 'com.egood.magtek-udynamo', 'isDeviceOpened', []
            );
        }

        $window.location.href = $rootScope.loginUrl;
    };
});

posControllers.controller('SwitchAssociateCtrl', function ($rootScope, $scope, $http) {
    $scope.pin = '';
    $scope.inputPinCode = function (num) {
        if (num == 'Back') {
            if ($scope.pin.length > 0) {
                $scope.pin = $scope.pin.substring(0, $scope.pin.length - 1);
            }
            return;
        }

        $scope.pin= $scope.pin.concat(num);
    };

    $scope.switchAssociate = function () {
        if (!$scope.pin) {
            $scope.error = "Please enter PIN #";

            return;
        }

        if ($scope.pin.length < 4) {
            $scope.error = "PIN # must be at least 4 characters";

            return;
        }

        var url = getURL($rootScope) + "/flow/POS.Associate.VerifyByPin";

        $http.get(url, {params: {'storeId': $rootScope.user.storeId, 'pin': $scope.pin, 'action': 'Switch User'}})
            .success(function (data, status, headers, config) {
                if (data && data.id > 0) {
                    $rootScope.user = data;
                    $scope.closeThisDialog(1);
                }
                else {
                    $scope.error = "Unauthorized";
                    $scope.pin = '';
                }
            })
            .error(function (data, status, headers, config) {
                $scope.error = "Unauthorized";
                $scope.pin = '';
            }
        );
    };
});

posControllers.controller('ShippingCtrl', function ($rootScope, $scope, $http, $cookieStore, ngDialog) {
    $scope.currentPage = 0;
    $scope.pageSize = 3;
    $scope.numberOfPages = 0;

    $scope.contentWeight = $scope.shippingDTO.weight;
    $scope.boxes = $scope.shippingDTO.boxes;

    $scope.populateBoxDimension = function (box) {
        if (box) {
            $scope.width = box.width;
            $scope.height = box.height;
            $scope.length = box.length;
            $scope.boxWeight = box.weight;

            $scope.totalWeight = $scope.boxWeight + $scope.contentWeight;
        }
    };

    $scope.calculateTotalWeight = function () {
        if ($scope.boxWeight >= 0 && $scope.contentWeight >= 0) {
            $scope.totalWeight = $scope.boxWeight + $scope.contentWeight;
        }
    };

    $scope.performRateShopping = function () {
        if ($scope.width < 0 || $scope.height < 0 || $scope.length < 0 || $scope.totalWeight < 0) {
            $scope.popupMsg = "Enter width, height, length, and total weight";
            ngDialog.open({scope: $scope});

            return;
        }

        var req = {
            'companyId': $rootScope.user.companyId,
            'company': $rootScope.user.company,
            'storeId': $scope.shippingDTO.storeId,
            'customer': $scope.shippingDTO.customer,
            'dimension': {
                'width': $scope.width,
                'height': $scope.height,
                'length': $scope.length,
                'weight': $scope.totalWeight
            }
        };

        $http.post(getURL($rootScope) + '/flow/POS.Store.Get Rate Shopping Results', req)
            .success(function (data, status, config, header) {
                if (data && data.length > 0) {
                    $scope.errorMsg = "";

                    var handlingFee = round($rootScope.storeProfile.shippingHandlingFee, 2);
                    for (var i = 0; i < data.length; i++) {
                        if (data[i]) {
                            data[i].cost += handlingFee;
                        }
                    }

                    $scope.rates = data;

                    $scope.numberOfPages = function () {
                        if ($scope.rates) {
                            return Math.ceil($scope.rates.length / $scope.pageSize);
                        }

                        return 1;
                    }
                }
                else {
                    $scope.errorMsg = "No Rate Shopping Results for a given customer.";
                }
            })
            .error(function (data, status, config, header) {
                $scope.errorMsg = "No Rate Shopping Results for a given customer.";
            }
        );
    };

    $scope.chooseShippingService = function (rate) {
        $scope.shipVendor = rate.carrier;
        $scope.shipVia = rate.shipVia;
        $scope.shipMethodCode = rate.code;
        $scope.cost = rate.cost;
    };

    $scope.acceptShipVia = function () {
        var response = {
            "carrier": $scope.shipVendor,
            "shipMethod": $scope.shipMethodCode,
            "shippingCost": $scope.cost
        };
        $scope.closeThisDialog(response);
    };
});

posControllers.controller('CheckoutCtrl', function ($rootScope, $scope, $http, $cookieStore, ngDialog, $q, $window, $timeout) {
    $scope.payAmount = "";
    $scope.paymentType = "";
    $scope.paymentId = -1;
    $scope.showDebitPinInput = false;
    $scope.emailReceiptFlag = $rootScope.storeProfile.emailReceipt;
    $scope.printReceiptFlag = $rootScope.storeProfile.printReceipt;
    $scope.payments = [];
    // $scope.storeCredit
    // $scope.paymentDue;
    $scope.btn_pay_clicked = false;
    $scope.btn_done_clicked = false;
    $scope.allowCreditCard = true;

    $scope.showOpenCashRegister = false;

    var cedIP = window.localStorage.getItem("cedIP");
    var orderNumber = '0000';

    $scope.numCardSwipes = 0;

    if ($scope.salesId > 0) {
        $http.get(getURL($rootScope) + '/flow/POS.Sales.GetPayments', {params: {'orderId': $scope.salesId}})
            .success(function (data, status, config, header) {
                if (data) {
                    for (var i = 0; i < data.length; i++) {
                        var pay = data[i];
                        if (pay.amount > 0) {
                            $scope.payments.push(pay);
                        }
                    }
                }
            })
            .error(function (data, status, config, header) {
            }
        );
        orderNumber = $scope.salesId;
    }

    if ($rootScope.currentCheckout && $rootScope.currentCheckout.items.length > 0 && "cayan" == $rootScope.storeProfile.gateway.type) {
        // close existing if there is any
        callExternalURL(cedIP + "/?Action=EndOrder&Order=" + orderNumber + "&ExternalPaymentType=Other&Format=JSON", function (data) {
            displayLines();
        });
    }

    function displayLines() {
        // start transaction
        var actions = [];

        actions.push(cedIP + "/?Action=StartOrder&Order=" + orderNumber + "&Format=JSON");
        var orderTotal = 0;
        var orderTaxTotal = 0;
        for (var i = 0; i < $rootScope.currentCheckout.items.length; i++) {
            var item = $rootScope.currentCheckout.items[i];

            var desc = item.description;
            if(desc.length > 35) {
                desc = desc.substring(0,34);
            }
            //desc = replaceAll(item.description, '/',' ');
            var itemDescription = encodeURIComponent(desc);
            var lineTotal = (item.lineSubtotal*item.quantity) - item.totalDiscount;
            var lineTaxTotal=0;
            if(item.tax > 0) {
                lineTaxTotal += item.tax;
            }
            if(item.cityTax > 0) {
                lineTaxTotal += item.cityTax;
            }

            orderTaxTotal = round(orderTaxTotal + lineTaxTotal,2);
            orderTotal = round(orderTotal + lineTotal + lineTaxTotal, 2);

            var sku = encodeURIComponent(item.sku + i.toString());
            var addItemURI = cedIP + "/?Action=AddItem&Order=" + orderNumber + "&Type=Misc&TypeValue="+sku+"&UPC="+sku+"&Quantity="+item.quantity;
            addItemURI += "&Description="+itemDescription+"&Amount="+lineTotal+"&TaxAmount="+lineTaxTotal+"&OrderTotal="+orderTotal+"&OrderTax="+orderTaxTotal+"&Category=None&Format=JSON";
            // add buy lines
            actions.push(addItemURI);
        }
        // sometimes order total and tax total are not getting updated correctly
        var updateTotalURI = cedIP + "/?Action=UpdateTotal&Order=" + orderNumber + "&OrderTotal="+orderTotal+"&OrderTax="+orderTaxTotal+"Format=JSON";
        actions.push(updateTotalURI);
        sendCallToCED(actions);
    }

    function replaceAll(str, find, replace) {
        return str.replace(new RegExp(find, 'g'), replace);
    }

    function sendCallToCED(actions) {
        if(actions.length > 0) {
            callExternalURLWithErrorCallBack(actions[0], 300,
                function (data) {
                    var result = JSON.parse(data);
                    if('Success' == result.Status) {
                        actions.shift();
                        sendCallToCED(actions);
                    }

                }, function (error) {
                    alert(error);
                });
        }
    }

    $scope.clear = function () {
        var canvas = document.getElementById('canvas');
        var context = canvas.getContext('2d');
        context.clearRect(0, 0, canvas.width, canvas.height)
    };

    $scope.inputKeyNum = function (num) {
        if (num == 'Back') {
            if ($scope.payAmount.length > 0) {
                $scope.payAmount = $scope.payAmount.substring(0, $scope.payAmount.length - 1);
            }
            return;
        }

        if ($scope.payAmount.indexOf('.') > 0 && (num == '.' || $scope.payAmount.indexOf('.') == $scope.payAmount.length - 3)) {
            return;
        }

        $scope.payAmount = $scope.payAmount.concat(num);
    };

    $scope.checkOutFinished = function() {
        return !angular.isUndefined($scope.salesId) && $scope.paymentDue <= 0;
    };

    $scope.copyTotalValueToAmount = function () {
        if ($scope.paymentDue > 0) {
            var tmp = $scope.paymentDue.toString();
            var index = tmp.indexOf('.');

            if (tmp.length - index > 2) {
                tmp = round(tmp, 2);
                tmp = tmp.toString();
                index = tmp.indexOf('.');
            }

            if (index == -1) {
                $scope.payAmount = tmp + '.00';
            }
            else {
                if (tmp.length - index <= 2) {
                    if (tmp.length - index == 2) {
                        $scope.payAmount = tmp + '0';
                    }
                    else {
                        $scope.payAmount = tmp + '00';
                    }
                }
                else {
                    $scope.payAmount = tmp;
                }
            }
        }
    };

    $scope.togglePaymentType = function (paymentType) {
        $scope.paymentType = paymentType;

        if ("Debit" == paymentType) {
            $scope.showDebitPinInput = true;
        }
        else {
            $scope.showDebitPinInput = false;
        }
    };

    $scope.openCashRegister = function () {
        openCashRegister();
    };

    $scope.hasToPay = function () {
        var hasToPay = (angular.isUndefined($scope.salesId) && $scope.paymentDue == 0) || $scope.total != null && $scope.total > 0 && $scope.payAmount != null && $scope.payAmount > 0 && $scope.paymentType != null && $scope.paymentType.length > 0;

        if (hasToPay) {
            if ($scope.paymentType == 'Credit') {
                if ($scope.ccManualEntry == true && $rootScope.storeProfile.gateway.type != 'cayan') {
                    // cc type, card, exp and cvv2 needs to be filled in.
                    if (!$scope.ccNumber || $scope.ccNumber.length < 10) {
                        return false;
                    }
                    if (!$scope.ccExpDate || $scope.ccExpDate.length < 7) {
                        return false;
                    }
                    if (!$scope.ccCVV2 || $scope.ccCVV2.length < 3) {
                        return false;
                    }
                }
            }

            if ($scope.paymentType != 'Cash') {
                return Number($scope.payAmount) <= Number($scope.paymentDue);
            }

            return true;
        }

        return hasToPay;
    };

    function resetTransactionScreen(isLayaway, approvedAmount) {
        $scope.paymentDue = round($scope.paymentDue - round(approvedAmount, 2), 2);
        $scope.payAmount = "";
        $scope.preventEdit = true;
        $scope.paymentType = "";
        $scope.showDebitPinInput = false;
        $scope.transactData = {};
        $scope.ccManualEntry = false;
        $scope.ccNumber = "";
        $scope.ccExpDate = "";
        $scope.ccCVV2 = "";
        $scope.carrier = "";
        $scope.shipMethod = "";

        if ($scope.salesId == null || $scope.salesId == "") {
            $scope.salesId = $rootScope.currentCheckout.salesId;
        }


        if (isLayaway || $scope.paymentDue <= 0) {
            var sessionKey = $cookieStore.get('sessionKey');
            var receiptFont = $rootScope.storeProfile.receiptFont;
            printReceipt(getURL($rootScope), sessionKey, $scope.salesId, true, $scope.printReceiptFlag, "sales", null, true, receiptFont);
            if ($scope.emailReceiptFlag) {
                $scope.emailReceipt();
            }

            $timeout(function(){ $scope.showOpenCashRegister = true;}, 10000);

            // fully paid.
            $rootScope.currentCheckout = {
                "items": [],
                "customer": {},
                "payments": [],
                "discountRate": 0
            };

            $scope.subtotal = 0;
            $scope.tax = "";
            $scope.total = "";
            $scope.paymentProcessingStatus = "Transaction completed";
            $scope.customerId = 0;
        }
    }

    $scope.emailReceipt = function () {
        if ($scope.salesId) {
            var url = getURL($rootScope) + "/flow/POS.Transact.EmailReceipt";
            $http.get(url, {params: {'saleId': $scope.salesId, 'customerEmail': $scope.customerEmail}}).
                success(function (data, status, headers, config) {
                }).
                error(function (data, status, headers, config) {
                    $scope.popupMsg = data;
                    ngDialog.open({scope: $scope});
                }
            );
        }
    };

    $scope.processPayment = function () {
        var allowCC = true;
        if ($scope.payments.length > 0) {
            allowCC = $scope.payments.length < 5;
        }

        if (allowCC == false) {
            $scope.paymentProcessingStatus = "You can use up to 5 payments Only.";

            return;
        }

        $scope.btn_pay_clicked = true;

        if ("Credit" == $scope.paymentType) {
            if ($scope.ccManualEntry == true && $rootScope.storeProfile.gateway.type != 'cayan') {
                var card_data = {
                    "ccNumber": $scope.ccNumber,
                    "ccExpDate": $scope.ccExpDate,
                    "ccCVV2": $scope.ccCVV2
                };

                var transact = {
                    "type": "CC",
                    "user": $rootScope.user,
                    "ccinfo": card_data,
                    "amount": $scope.payAmount,
                    "paymentDue": $scope.paymentDue,
                    "carrier": $scope.carrier,
                    "shipMethod": $scope.shipMethod,
                    "totals": {
                        "total": $scope.total,
                        "subtotal": $scope.subtotal,
                        "totalDiscount": $scope.totalDiscount,
                        "shippingCost": $scope.shippingCost,
                        "tax": $scope.tax,
                        "cityTax": $scope.cityTax
                    },
                    "lines": $rootScope.currentCheckout.items,
                    "customerId": $scope.customerId,
                    "salesId": $scope.salesId,
                    "notes": $scope.notes
                };

                $scope.transactData = transact;
                $scope.showSignaturePad = true;
                $scope.paymentProcessingStatus = "SIGN BELOW";
            }
            else {
                if ("cayan" == $rootScope.storeProfile.gateway.type) {
                    var transact = {
                        "type": "CC",
                        "user": $rootScope.user,
                        "amount": $scope.payAmount,
                        "paymentDue": $scope.paymentDue,
                        "carrier": $scope.carrier,
                        "shipMethod": $scope.shipMethod,
                        "totals": {
                            "total": $scope.total,
                            "subtotal": $scope.subtotal,
                            "totalDiscount": $scope.totalDiscount,
                            "shippingCost": $scope.shippingCost,
                            "tax": $scope.tax,
                            "cityTax": $scope.cityTax
                        },
                        "lines": $rootScope.currentCheckout.items,
                        "customerId": $scope.customerId,
                        "salesId": $scope.salesId,
                        "notes": $scope.notes
                    };

                    var url = getURL($rootScope) + "/flow/POS.Transact.ProcessPayment";
                    $http.post(url, transact).
                        success(function (data, status, headers, config) {
                            $scope.paymentProcessingStatus = "In Progress";
                            var txKey = data.orderId;

                            if (txKey) {
                                $scope.salesId = data.salesId;

                                var cedIP = window.localStorage.getItem("cedIP");

                                if ($scope.ccManualEntry == true) {
                                    $timeout(function () {
                                        callExternalURL(cedIP + "?Action=InitiateKeyedSale&Format=JSON", function (data) {
                                        });
                                    }, 4000, false);
                                }

                                var timeOut = 300;
                                if($rootScope.storeProfile.cedTimeOut && $rootScope.storeProfile.cedTimeOut != null && $rootScope.storeProfile.cedTimeOut > 0) {
                                    timeOut = $rootScope.storeProfile.cedTimeOut;
                                }

                                callExternalURLWithErrorCallBack(cedIP + "?TransportKey=" + txKey + "&Format=JSON",timeOut,
                                    function (data) {
                                        if (data) {
                                            var responseData = data;
                                            var json = JSON.parse(data);
                                            json.salesId = $scope.salesId;
                                            json.paymentDue = $scope.paymentDue;
                                            json.userId = $rootScope.user.id;
                                            json.storeId = $rootScope.user.storeId;
                                            json.storeName = $rootScope.user.storeName;
                                            json.transportKey = txKey;

                                            $http.post(getURL($rootScope) + "/flow/POS.Transact.ConfirmCayanPayment", json).
                                                success(function (data, status, headers, config) {
                                                    $scope.btn_pay_clicked = false;

                                                    if (json && json.Status == "APPROVED") {
                                                        $scope.paymentProcessingStatus = "Accepted";

                                                        var totalPaid = round(json.AmountApproved, 2);
                                                        if (json.AdditionalParameters && json.AdditionalParameters.AmountDetails && json.AdditionalParameters.AmountDetails.Cashback) {
                                                            var cashback = round(json.AdditionalParameters.AmountDetails.Cashback, 2);
                                                            if (cashback > 0) {
                                                                totalPaid = totalPaid - cashback;
                                                                $scope.paymentProcessingStatus += ": Cashback ($" + cashback + ")";
                                                            }
                                                        }

                                                        $scope.payments.push({"amount": totalPaid, "method": "CC"});
                                                        resetTransactionScreen(false, totalPaid);
                                                        $scope.$apply();
                                                    }
                                                    else {
                                                        $scope.numCardSwipes = $scope.numCardSwipes + 1;
                                                        if($rootScope.storeProfile.maxCardSwipes && $rootScope.storeProfile.maxCardSwipes > 0 && $scope.numCardSwipes > $rootScope.storeProfile.maxCardSwipes) {
                                                            $scope.allowCreditCard = false;
                                                            $scope.paymentType = "";
                                                            $scope.payAmount = 0;
                                                        }

                                                        // increase failed swipes
                                                        if (json.Status == 'UserCancelled') {
                                                            $scope.paymentProcessingStatus = "User Canceled";
                                                        }
                                                        else {
                                                            $scope.paymentProcessingStatus = json.ErrorMessage;
                                                        }

                                                        //log error
                                                        var url = getURL($rootScope) + "/flow/POS.Log.CreateLog";
                                                        var sessionKey = $cookieStore.get('sessionKey');
                                                        var message = 'global.js:ConfirmCayanPayment_notApproved:'+data+':'+responseData;
                                                        $http.get(url, {params: {'sessionKey':sessionKey,'severity':'Error','message':message}})
                                                            .success(function (data, status, headers, config) {
                                                                //
                                                            }).
                                                            error(function (data, status, headers, config) {
                                                                //
                                                            }
                                                        );
                                                        $scope.$apply();
                                                    }
                                                }).
                                                error(function (data, status, headers, config) {
                                                    $scope.paymentProcessingStatus = data;
                                                    $scope.$apply();
                                                    $scope.btn_pay_clicked = false;
                                                    //log error
                                                    var url = getURL($rootScope) + "/flow/POS.Log.CreateLog";
                                                    var sessionKey = $cookieStore.get('sessionKey');
                                                    var message = 'global.js:ConfirmCayanPayment_notApproved:'+data+':'+responseData;
                                                    $http.get(url, {params: {'sessionKey':sessionKey,'severity':'Error','message':message}})
                                                        .success(function (data, status, headers, config) {
                                                            //
                                                        }).
                                                        error(function (data, status, headers, config) {
                                                            //
                                                        }
                                                    );
                                                }
                                            );
                                        }
                                        else {
                                            $scope.btn_pay_clicked = false;
                                        }
                                    },
                                    function(error) {
                                        $scope.paymentProcessingStatus = "Error while waiting CED response";
                                        $scope.$apply();
                                        $scope.btn_pay_clicked = false;

                                        if(error.indexOf("The request timed out") > -1) {
                                            var cedIP = window.localStorage.getItem("cedIP");

                                            callExternalURL(cedIP + "?Action=Cancel&Format=JSON", function (data) {
                                                if (data) {
                                                    //alert(data);
                                                }
                                            });

                                            //alert("Error while calling external URL : " + error);
                                            // we need to check if the transaction already went through and if it did then we need to create a payment record in Deposco
                                            var url = getURL($rootScope) + "/flow/POS.Transact.CheckCEDTimeOut";
                                            var storeId = $rootScope.user.storeId;
                                            $http.get(url, {params: {'userId':$rootScope.user.id,'storeId': storeId,'transportKey': txKey,'salesId':$scope.salesId,'paymentDue':$scope.paymentDue}})
                                                .success(function (data, status, headers, config) {
                                                    if(data.totalAmountApproved > 0) {
                                                        $scope.paymentProcessingStatus = "Accepted";

                                                        var totalPaid = round(data.totalAmountApproved, 2);

                                                        $scope.payments.push({"amount": totalPaid, "method": "CC"});
                                                        resetTransactionScreen(false, totalPaid);
                                                        $scope.$apply();
                                                    }
                                                }).
                                                error(function (data, status, headers, config) {
                                                    //alert('Error while calling Check CED time out');
                                                }
                                            );
                                        }
                                        else {
                                            var url = getURL($rootScope) + "/flow/POS.Log.CreateLog";
                                            var sessionKey = $cookieStore.get('sessionKey');
                                            var message = 'global.js:CED_ERROR:'+error;
                                            $http.get(url, {params: {'sessionKey':sessionKey,'severity':'Error','message':message}})
                                                .success(function (data, status, headers, config) {
                                                    //
                                                }).
                                                error(function (data, status, headers, config) {
                                                        //
                                                }
                                            );

                                        }
                                    }
                                );
                            }
                            else {
                                $scope.btn_pay_clicked = false;
                            }
                        }).
                        error(function (data, status, headers, config) {
                            $scope.paymentProcessingStatus = data;
                        }
                    );
                }
                else {
                    if (window.cordova.plugins.MagTek) {
                        cordova.exec(function success(value) {
                            if (!value) {
                                cordova.exec(
                                    function success(value) {
                                    },
                                    function failed(value) {
                                    }, 'com.egood.magtek-udynamo', 'openDevice', []
                                );
                            }
                        }, function failed() {
                        }, 'com.egood.magtek-udynamo', 'isDeviceOpened', []);

                        $scope.showSignaturePad = false;
                        var context = canvas.getContext('2d');
                        context.clearRect(0, 0, canvas.width, canvas.height)

                        $scope.paymentProcessingStatus = "Swipe Credit Card now";
                        $scope.$apply();

                        $timeout(function () {
                            cordova.exec(function readOK(card_data) {
                                $scope.btn_pay_clicked = false;

                                if ("00" != card_data["Card.Status"]) {
                                    $scope.transactData = {};
                                    $scope.paymentProcessingStatus = "Credit Card is not read correctly. Try again [" + card_data["Card.Status"] + "]";
                                    $scope.$apply();
                                }
                                else {
                                    var transact = {
                                        "type": "CC",
                                        "user": $rootScope.user,
                                        "ccinfo": card_data,
                                        "amount": $scope.payAmount,
                                        "paymentDue": $scope.paymentDue,
                                        "carrier": $scope.carrier,
                                        "shipMethod": $scope.shipMethod,
                                        "totals": {
                                            "total": $scope.total,
                                            "subtotal": $scope.subtotal,
                                            "totalDiscount": $scope.totalDiscount,
                                            "shippingCost": $scope.shippingCost,
                                            "tax": $scope.tax,
                                            "cityTax": $scope.cityTax
                                        },
                                        "lines": $rootScope.currentCheckout.items,
                                        "customerId": $scope.customerId,
                                        "salesId": $scope.salesId,
                                        "notes": $scope.notes
                                    };

                                    $scope.transactData = transact;
                                    $scope.showSignaturePad = true;
                                    $scope.paymentProcessingStatus = "SIGN BELOW";
                                    $scope.$apply();
                                }

                                cordova.exec(
                                    function success(value) {
                                        if (value) {
                                            cordova.exec(
                                                function success(value) {
                                                },
                                                function failed() {
                                                }, 'com.egood.magtek-udynamo', 'closeDevice', []
                                            );
                                        }
                                    }, function failed() {
                                    }, 'com.egood.magtek-udynamo', 'isDeviceOpened', []
                                );
                            }, function failToRead(status) {
                                window.alert("Fail to read:" + status);
                            }, 'com.egood.magtek-udynamo', 'listenForEvents', ['TRANS_EVENT_OK', 'TRANS_EVENT_ERROR']);
                        }, 800, false);
                    }
                    else {
                        $scope.paymentProcessingStatus = "Please attach the card reader";
                        $scope.btn_pay_clicked = false;
                    }
                }
            }
        }
        else if ("Cash" == $scope.paymentType) {
            $scope.paymentProcessingStatus = "In Progress";

            var transact = {
                "type": "CS",
                "user": $rootScope.user,
                "amount": $scope.payAmount,
                "paymentDue": $scope.paymentDue,
                "carrier": $scope.carrier,
                "shipMethod": $scope.shipMethod,
                "totals": {
                    "total": $scope.total,
                    "subtotal": $scope.subtotal,
                    "totalDiscount": $scope.totalDiscount,
                    "shippingCost": $scope.shippingCost,
                    "tax": $scope.tax,
                    "cityTax": $scope.cityTax
                },
                "lines": $rootScope.currentCheckout.items,
                "customerId": $scope.customerId,
                "salesId": $scope.salesId,
                "notes": $scope.notes
            };

            var url = getURL($rootScope) + "/flow/POS.Transact.ProcessPayment";
            $http.post(url, transact).
                success(function (data, status, headers, config) {
                    $scope.btn_pay_clicked = false;

                    $scope.salesId = data.salesId;
                    $scope.paymentProcessingStatus = "Accepted";
                    $scope.payments.push({"amount": $scope.payAmount, "method": "CS"});
                    resetTransactionScreen(false, $scope.payAmount);
                }).
                error(function (data, status, headers, config) {
                    $scope.btn_pay_clicked = false;

                    $scope.paymentProcessingStatus = data;
                }
            );
        }
        else if ("External" == $scope.paymentType) {
            $scope.paymentProcessingStatus = "In Progress";

            var transact = {
                "type": "EX",
                "user": $rootScope.user,
                "amount": $scope.payAmount,
                "paymentDue": $scope.paymentDue,
                "carrier": $scope.carrier,
                "shipMethod": $scope.shipMethod,
                "totals": {
                    "total": $scope.total,
                    "subtotal": $scope.subtotal,
                    "totalDiscount": $scope.totalDiscount,
                    "shippingCost": $scope.shippingCost,
                    "tax": $scope.tax,
                    "cityTax": $scope.cityTax
                },
                "lines": $rootScope.currentCheckout.items,
                "customerId": $scope.customerId,
                "salesId": $scope.salesId,
                "notes": $scope.notes
            };

            var url = getURL($rootScope) + "/flow/POS.Transact.ProcessPayment";
            $http.post(url, transact).
                success(function (data, status, headers, config) {
                    $scope.btn_pay_clicked = false;

                    $scope.salesId = data.salesId;
                    $scope.paymentProcessingStatus = "Accepted";
                    $scope.payments.push({"amount": $scope.payAmount, "method": "EX"});
                    resetTransactionScreen(false, $scope.payAmount);
                }).
                error(function (data, status, headers, config) {
                    $scope.paymentProcessingStatus = data;
                    $scope.btn_pay_clicked = false;
                }
            );
        }
        else if ("Store" == $scope.paymentType) {
            if ($scope.payAmount > $scope.storeCredit) {
                $scope.paymentProcessingStatus = "Exceeds SC: $" + $scope.storeCredit;
                $scope.$apply();

                $scope.btn_pay_clicked = false;
                return;
            }

            $scope.paymentProcessingStatus = "In Progress";
            //$scope.$apply();

            var transact = {
                "type": "SC",
                "user": $rootScope.user,
                "amount": $scope.payAmount,
                "paymentDue": $scope.paymentDue,
                "carrier": $scope.carrier,
                "shipMethod": $scope.shipMethod,
                "totals": {
                    "total": $scope.total,
                    "subtotal": $scope.subtotal,
                    "totalDiscount": $scope.totalDiscount,
                    "shippingCost": $scope.shippingCost,
                    "tax": $scope.tax,
                    "cityTax": $scope.cityTax
                },
                "lines": $rootScope.currentCheckout.items,
                "customerId": $scope.customerId,
                "salesId": $scope.salesId,
                "notes": $scope.notes
            };

            var url = getURL($rootScope) + "/flow/POS.Transact.ProcessPayment";
            $http.post(url, transact).
                success(function (data, status, headers, config) {
                    $scope.btn_pay_clicked = false;

                    $scope.salesId = data.salesId;
                    $scope.paymentProcessingStatus = "APPLIED";
                    $scope.storeCredit -= $scope.payAmount;
                    $scope.payments.push({"amount": $scope.payAmount, "method": "SC"});
                    resetTransactionScreen(false, $scope.payAmount);
                }).
                error(function (data, status, headers, config) {
                    $scope.paymentProcessingStatus = data;
                    $scope.$apply();

                    $scope.btn_pay_clicked = false;
                }
            );
        }
    };

    $scope.commitTransaction = function () {
        if (!$scope.transactData.amount) {
            $scope.transactData = {};
            $scope.paymentProcessingStatus = "Credit Card is not read correctly. Try again [" + card_data["Card.Status"] + "]";
            $scope.$apply();

            $scope.btn_pay_clicked = false;

            return;
        }

        $scope.btn_done_clicked = true;

        var canvas = document.getElementById('canvas');
        var sigImage = canvas.toDataURL();

        $scope.paymentProcessingStatus = "In Progress";
        $scope.$apply();
        $scope.transactData.signature = sigImage;

        var url = getURL($rootScope) + "/flow/POS.Transact.ProcessPayment";
        $http.post(url, $scope.transactData).
            success(function (data, status, headers, config) {
                $scope.btn_pay_clicked = false;
                $scope.btn_done_clicked = false;

                $scope.paymentProcessingStatus = "CREDIT SALE POSTED";
                $scope.showSignaturePad = false;
                var context = canvas.getContext('2d');
                context.clearRect(0, 0, canvas.width, canvas.height)

                if (data.approved) {
                    $scope.transactData = {};
                    $scope.salesId = data.salesId;
                    $scope.payments.push({"amount": $scope.payAmount, "method": "CC"});
                    resetTransactionScreen(false, $scope.payAmount);
                    $scope.ccManualEntry = false;
                    $scope.ccNumber = "";
                    $scope.ccExpDate = "";
                    $scope.ccCVV2 = "";
                }
                else {
                    $scope.transactData = {};
                    if (data.salesId) {
                        $scope.salesId = data.salesId;
                        $rootScope.currentCheckout.salesId = data.salesId;
                    }

                    if (data.result.length > 1) {
                        $scope.paymentProcessingStatus = data.result;
                    }
                    else {
                        $scope.paymentProcessingStatus = "Declined. Try again or with different card.";
                    }

                    $scope.$apply();
                    // $scope.transactData = {};
                }

                $scope.$apply();
            }).
            error(function (data, status, headers, config) {
                $scope.btn_pay_clicked = false;
                $scope.btn_done_clicked = false;

                $scope.paymentProcessingStatus = "Error with post sales. Please try again.";
                $scope.$apply();
                // $scope.transactData = {};
            }
        );
    };

    $scope.smartClose = function () {
        var cedIP = window.localStorage.getItem("cedIP");
        if ("cayan" == $rootScope.storeProfile.gateway.type) {
            callExternalURL(cedIP + "/?Action=EndOrder&Order=" + orderNumber + "&ExternalPaymentType=Other&Format=JSON", function (data) {
            });
        }
        if ("Credit" == $scope.paymentType && "cayan" == $rootScope.storeProfile.gateway.type) {
            var cedIP = window.localStorage.getItem("cedIP");
            callExternalURL(cedIP + "?Action=Cancel&Format=JSON", function (data) {
                if (data) {
                    //alert(data);
                }
            });
        }
        $scope.closeThisDialog($scope.payments.length);
    };

    $scope.layaway = function () {
        var paidAmount = $scope.total - $scope.paymentDue;
        var minAmountForLayaway = $scope.total * ($rootScope.storeProfile.layawayPercentage * 0.01);

        if ("cayan" == $rootScope.storeProfile.gateway.type) {
            callExternalURL(cedIP + "/?Action=EndOrder&Order=" + orderNumber + "&ExternalPaymentType=Other&Format=JSON", function (data) {
            });
        }

        if (minAmountForLayaway > paidAmount) {
            $scope.paymentProcessingStatus = "You need to pay more than $" + minAmountForLayaway + " to layaway";
            return;
        }

        resetTransactionScreen(true, $scope.payAmount);

        $scope.closeThisDialog(1);
        //$scope.closeThisDialog(Number($scope.paymentDue));
    }
});


function idlePopup() {
    var t;
    window.onload = resetTimer;
    window.onmousemove = resetTimer;
    window.onmousedown = resetTimer; // catches touchscreen presses
    window.onclick = resetTimer;     // catches touchpad clicks
    window.onscroll = resetTimer;    // catches scrolling with arrow keys
    window.onkeypress = resetTimer;

    function popUp() {
        //
    }

    function resetTimer() {
        clearTimeout(t);
        t = setTimeout(popUp(), 10000);  // time is in milliseconds
    }
}

//idlePopup();
