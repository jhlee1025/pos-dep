'use strict';

var posControllers = angular.module('posCustomerControllers', []);

posControllers.controller('CustomerCtrl', function ($rootScope, $scope, $route, $location, $http) {
    $scope.currentPage = 0;
    $scope.pageSize = 5;
    $scope.numberOfPages = 0;

    initializeCurrentCheckout($rootScope);

    $scope.lookupCustomer = function () {
        if ($scope.phoneNumber) {
            $scope.phoneNumber = $scope.phoneNumber.replace(/-/g, "");
        }

        if ($scope.phoneNumber || $scope.email || $scope.customerName || $scope.dob || $scope.zipCode || $scope.driverLicense) {
            // has at least one parameter
            var searchParams = {
                'phoneNumber': $scope.phoneNumber,
                'email': $scope.email,
                'customerName': $scope.customerName,
                'dob': $scope.dob,
                'zipCode': $scope.zipCode,
                'driverLicense': $scope.driverLicense
            };

            $http.get(getURL($rootScope) + "/flow/POS.Customer.Search", { //customers/search
                params: searchParams
            }).success(function (data, status, headers, config) {
                if (data) {
                    $scope.customers = data;

                    for (var i = 0; i < $scope.customers.length; i++) {
                        var customer = $scope.customers[i];
                        if (!customer.firstName && !customer.lastName) {
                            customer.firstName = "Undefined";
                        }
                    }
                    $scope.numberOfPages = function () {
                        if ($scope.customers) {
                            return Math.ceil($scope.customers.length / $scope.pageSize);
                        }

                        return 1;
                    };
                }
            }).error(function (data, status, headers, config) {
                console.log("No customer");
                $scope.customers = [];
            });
        }
    };

    $scope.selectCustomer = function (customer) {
        $rootScope.currentCheckout.customer = customer;
        $location.path('/checkout');
    };

    $scope.viewCustomerDetail = function (customerId) {
        if (customerId > 0) {
            /* Converts an object into a key/value par with an optional prefix. Used for converting objects to a query string */
            var searchParams = {
                'cid': customerId,
                'phoneNumber': $scope.phoneNumber,
                'email': $scope.email,
                'customerName': $scope.customerName,
                'dob': $scope.dob,
                'zipCode': $scope.zipCode,
                'driverLicense': $scope.driverLicense
            };

            var queryString = qs(searchParams);
            $location.path('/customer_detail').search(queryString);
        }
    };

    var cid = $location.search().cid;
    if (cid) {
        $scope.phoneNumber = $location.search().phoneNumber;
        $scope.email = $location.search().email;
        $scope.customerName = $location.search().customerName;
        $scope.dob = $location.search().dob;
        $scope.zipCode = $location.search().zipCode;
        $scope.driverLicense = $location.search().driverLicense;

        $scope.lookupCustomer();
    }

    $location.$$search = {};
});

posControllers.controller('NewCustomerCtrl', function ($rootScope, $scope, $route, $http, $window, $location, ngDialog) {
    $scope.customer = {};
    $scope.imageUrl = '';
    $scope.createButtonDisabled = false;

    $scope.countries;

    var url2 = getURL($rootScope) + "/flow/POS.Customer.GetCountries";
    $http.get(url2, {}).
        success(function (data, status, headers, config) {
            $scope.countries = data;
        }).
        error(function (data, status, headers, config) {
            $scope.popupMsg = "Error: " + data;
            ngDialog.open({scope: $scope});
        });

    if(!angular.isUndefined($rootScope.phoneNumber) && $rootScope.phoneNumber != null && $rootScope.phoneNumber != '') {
        $scope.customer.phoneNumber = Number($rootScope.phoneNumber);
    }

    $scope.submit = function (customer) {

        if($scope.createButtonDisabled) return;

        $scope.message = "";

        if (customer.phoneNumber == null) {
            $scope.message = "Phone number is required";

            return;
        }

        $http.put(getURL($rootScope) + "/flow/POS.Customer.CreateNewCustomer", customer)
            .success(function (data, status, headers, config) {
                $scope.customer = {};
                $scope.message = "New user is added";

                $rootScope.currentCheckout.customer = data;
                $location.path('/checkout');
            })
            .error(function (data, status, headers, config) {
                $scope.popupMsg = "Error: " + data;
                ngDialog.open({scope: $scope});
            }
        );
        $rootScope.phoneNumber='';
        $scope.createButtonDisabled = true;

    };

    $scope.goBack = function () {
        window.history.back();
    };

    $scope.takePicture = function () {
        if (navigator.camera) {
            navigator.camera.getPicture(
                function (imageData) {
                    var image = document.getElementById('customerImageID');
                    image.src = "data:image/jpeg;base64," + imageData;
                    $scope.customer.image = imageData;
                },
                function (message) {
                    $scope.popupMsg = "Unable to capture the photo";
                    ngDialog.open({scope: $scope});
                },
                {
                    quality: 75,
                    destinationType: Camera.DestinationType.DATA_URL,
                    cameraDirection: Camera.Direction.FRONT,
                    encodingType: Camera.EncodingType.JPEG,
                    targetWidth: 600,
                    targetHeight: 600
                }
            );
        }
        else {
            $scope.processMsg = "Only works with tablet";
        }
    };

    function parseData(data, token) {
        var i = data.indexOf(token);
        if (i != -1) {
            var j = data.indexOf('\n', i + token.length);
            if (i != -1 && j != -1 && i <= j) {
                return data.substring(i + token.length, j);
            }
        }

        return "";
    }

    $scope.parseDriverLicense = function () {
        if ($scope.customer.driverLicense && $scope.customer.driverLicense.length > 50) {
            var data = $scope.customer.driverLicense;
            $scope.customer.lastName = parseData(data, "DCS");
            $scope.customer.firstName = parseData(data, "DAC");
            $scope.customer.middleName = parseData(data, "DAD");

            if ($scope.customer.firstName == null && $scope.customer.middleName == null) {
                var givenName = parseData(data, "DCT");
                if (givenName) {
                    var givenNameIndex = givenName.indexOf(" ");
                    if (givenNameIndex != -1) {
                        $scope.customer.firstName = givenName.substring(0, givenNameIndex);
                        $scope.customer.middleName = givenName.substring(givenNameIndex + 1);
                    }
                    else {
                        $scope.customer.firstName = givenName;
                    }
                }
            }
            $scope.customer.dob = parseData(data, "DBB");
            $scope.customer.line1 = parseData(data, "DAG");
            $scope.customer.city = parseData(data, "DAI");
            $scope.customer.state = parseData(data, "DAJ");
            $scope.customer.zipCode = parseData(data, "DAK");
            $scope.customer.country = parseData(data, "DCG");
            var gender = parseData(data, "DBC");
            if (gender == "1") {
                $scope.customer.gender = "Male";
            }
            else {
                $scope.customer.gender = "Female";
            }

            $scope.customer.driverLicense = parseData(data, "DAQ");
        }
    };
});

posControllers.controller('CustomerDetailCtrl', function ($rootScope, $scope, $location, $window, $http, ngDialog) {
    $scope.action = "Edit Profile";
    $scope.canEdit = false;
    var customerId = $location.search().cid;
    var fromPage = $location.search().fromPage;

    $scope.customer = {};
    if (customerId) {
        var url = getURL($rootScope) + "/flow/POS.Customer.GetProfile";
        $http.get(url, {params: {'customerId': customerId}}).
            success(function (data, status, headers, config) {
                $scope.customer = data;
            }).
            error(function (data, status, headers, config) {
                $scope.popupMsg = "Error: " + data;
                ngDialog.open({scope: $scope});
            });
    }

    $scope.countries;

    var url2 = getURL($rootScope) + "/flow/POS.Customer.GetCountries";
    $http.get(url2, {}).
        success(function (data, status, headers, config) {
            $scope.countries = data;
        }).
        error(function (data, status, headers, config) {
            $scope.popupMsg = "Error: " + data;
            ngDialog.open({scope: $scope});
        });

    $scope.goBack = function () {
        var queryString = qs($location.search());

        if (fromPage == null) {
            fromPage = "customers";
        }

        $location.path('/' + fromPage).search(queryString);
    };

    $scope.enableOrSave = function () {
        if (!$scope.canEdit) {
            $scope.canEdit = !$scope.canEdit;
            $scope.action = "Save";
        }
        else {
            $scope.canEdit = !$scope.canEdit;
            $scope.action = "Edit Profile";

            var url = getURL($rootScope) + "/flow/POS.Customer.UpdateCustomerProfile";

            $http.post(url, $scope.customer)
                .success(function (data, status, headers, config) {
                    $scope.message = "Saved";
                })
                .error(function (data, status, headers, config) {
                    $scope.popupMsg = "Error: " + data + ". Please try again";
                    ngDialog.open({scope: $scope});
                }
            );
        }
    };

    $scope.takePicture = function () {
        if (navigator.camera) {
            navigator.camera.getPicture(
                function (imageData) {
                    var image = document.getElementById('customerImageID');
                    image.src = "data:image/jpeg;base64," + imageData;
                    $scope.customer.image = imageData;
                    $scope.$apply();
                },
                function (message) {
                    $scope.popupMsg = "Unable to capture photo [" + message + "]";
                    ngDialog.open({scope: $scope});
                },
                {
                    quality: 75,
                    destinationType: Camera.DestinationType.DATA_URL,
                    cameraDirection: Camera.Direction.FRONT,
                    encodingType: Camera.EncodingType.JPEG,
                    targetWidth: 600,
                    targetHeight: 600
                }
            );
        }
        else {
            $scope.processMsg = "Only works with tablet";
        }
    };

    $scope.deleteCustomer = function (customerId) {
        $http.delete(getURL($rootScope) + '/flow/POS.Customer.DeleteCustomer', {params: {'customerId': customerId}})
            .success(function (data, status, header, config) {
                $scope.customer = {};
                $scope.message = "The customer is removed";
            })
            .error(function (data, status, header, config) {
                $scope.popupMsg = "Unable to remove the customer. Please try again.";
                ngDialog.open({scope: $scope});
            }
        );
    };
});
