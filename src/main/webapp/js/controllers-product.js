'use strict';

var posControllers = angular.module('posProductControllers', []);

posControllers.controller('ProductCtrl', function ($rootScope, $scope, $route, $http, $window, ngDialog) {
    initializeCurrentCheckout($rootScope);

    $scope.categories = [];
    $scope.products = [];
    $scope.currentPage = 0;
    $scope.pageSize = 9;
    $scope.numberOfPages = 0;

    $scope.productImage = '';

    var url = getURL($rootScope) + "/flow/POS.Item.GetAllCategories";

    $http.get(url).
        success(function (data, status, headers, config) {
            if (data) {
                $scope.categories = data.categories;
            }
        }).
        error(function (data, status, headers, config) {
            $scope.categories = [];
        }
    );

    $scope.searchBySku = function (itemNumber) {
        var canSearch = itemNumber && itemNumber.length > 0;

        if (canSearch) {
            $scope.searchProducts();
        }

    };

    $scope.searchProducts = function () {
        var canSearch = $scope.searchCriteria && $scope.searchCriteria.length > 0;
        if (!canSearch) {
            if($scope.categories.length > 0) {
                for (var i = 0; i < $scope.categories.length; i++) {
                    if($scope.categories[i] != null) {
                        canSearch = true;
                        break;
                    }
                }
            }
        }
        if (canSearch) {
            // has at least one parameter
            var url = getURL($rootScope) + "/flow/POS.Item.GenericSearch";

            $http.get(url, {params: {
                'categories': $scope.categories,
                'storeOnly': $scope.storeOnly,
                'storeId': $rootScope.user.storeId,
                'searchCriteria': $scope.searchCriteria}}).
                success(function (data, status, headers, config) {
                    if (data) {
                        $scope.products = data;
                        $scope.currentPage = 0;
                        $scope.numberOfPages = function () {
                            if ($scope.products) {
                                return Math.ceil($scope.products.length / $scope.pageSize);
                            }

                            return 1;
                        }
                    }
                    else {
                        $scope.products = [];
                        $scope.popupMsg = "No product";
                        ngDialog.open({scope: $scope});
                    }
                }).
                error(function (data, status, headers, config) {
                    $scope.products = [];
                    $scope.popupMsg = "No product";
                    ngDialog.open({scope: $scope});
                }
            );
        }
        else {
            $scope.popupMsg = "Enter Product SKU (4 minimum) or choose Filter(s)";
            ngDialog.open({scope: $scope});
        }
    };

    $scope.addToCheckout = function (product) {
        var prod = angular.copy(product);

        var qtyOnCart = getQtyOnCart(product.storeId, product.sku);
        if(qtyOnCart >= product.atp) {
            $scope.popupMsg = "[" + product.sku + "] is not available";
            ngDialog.open({scope: $scope});
            return;
        }

        product.action = "Added";
        prod.discountRate = '';
        prod.discountTotal = '';
        prod.finalPrice = product.price;
        $rootScope.currentCheckout.items.push(prod);
    };

    function getQtyOnCart(storeId, sku) {
        var qtyOnCart=0;
        if ($rootScope.currentCheckout && $rootScope.currentCheckout.items.length > 0) {
            for (var i = 0; i < $rootScope.currentCheckout.items.length; i++) {
                var item = $rootScope.currentCheckout.items[i];
                if(item.storeId == storeId && item.sku == sku) {
                    qtyOnCart += item.quantity;
                }
            }
        }
        return qtyOnCart;
    }

    $scope.displayProductImage = function (product) {
        $scope.productImage = product.imageUrl;
        ngDialog.open({template: 'templates/display-image.html', scope: $scope, className: 'ngdialog-theme-mine'});
    };

    $scope.orderByValue = function(v) {
        return v;
    }
});