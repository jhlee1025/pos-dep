'use strict';

var posControllers = angular.module('posGiftControllers', []);

posControllers.controller('GiftCtrl', function ($rootScope, $scope, $http, ngDialog) {
    $scope.checkBalance = function (cardNumber) {
        if (cardNumber) {
            $http.get(getURL($rootScope) + '/flow/POS.Transact.CheckGiftCardBalance', {params: {'cardNumber': cardNumber, 'storeId': $rootScope.user.storeId}})
                .success(function (data, status, config, header) {
                    if (data) {
                        $scope.cardBalance = data.cardBalance;
                        $scope.pointsBalance = data.pointsBalance;
                        $scope.lifePointsBalance = data.lifePointsBalance;
                        $scope.expirationDate = data.expirationDate;
                    }
                    else {
                        $scope.processMsg = "No results";
                    }
                })
                .error(function (data, status, config, header) {
                    $scope.processMsg = data;
                }
            );
        }
    };
});