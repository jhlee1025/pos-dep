'use strict';

var posControllers = angular.module('posSetupControllers', []);

function callExternalURL(url, callBackFunction) {
    if (window.cordova !== undefined) {
        cordova.exec(
            function callback(data) {
                callBackFunction(data);
            },
            // Register the errorHandler
            function errorHandler(err) {
                alert("Error calling : " + err);
            },
            'POSUtil',
            'callExternalURL',
            [url, 300]
        );
    }
}

posControllers.controller('SetupCtrl', function ($controller, $cookieStore, $rootScope, $scope, $http, ngDialog) {
    $scope.panes = [
        {
            "name": "Store",
            "template": "templates/setup-store-pane.html",
            "controller": "SetupStoreCtrl",
            "includedInTabView": true
        },
        {
            "name": "Staff Emails",
            "template": "templates/setup-staff-pane.html",
            "controller": "SetupStaffCtrl",
            "includedInTabView": true
        },
        {
            "name": "Payment Gateway",
            "template": "templates/setup-gateway-pane.html",
            "controller": "SetupGatewayCtrl",
            "includedInTabView": true
        },
        {
            "name": "Printer",
            "template": "templates/setup-printer-pane.html",
            "controller": "SetupPrinterCtrl",
            "includedInTabView": true
        }
    ];

    if ($rootScope.user.admin) {
        $scope.panes.push(
            {
                "name": "Associates",
                "template": "templates/setup-associates-pane.html",
                "controller": "SetupAssociatesCtrl",
                "includedInTabView": true

            }
        );
    }

    if ($scope.storeProfile != null && $scope.storeProfile.printIvuLoto) {
        $scope.panes.push(
            {
                "name": "IVU Loto",
                "template": "templates/setup-ivu-loto-pane.html",
                "controller": "SetupIvuLotoCtrl",
                "includedInTabView": true

            }
        );
    }
});

function getStoreProfile($window, $http, $rootScope, $scope, ngDialog) {
    var url = getURL($rootScope) + "/flow/POS.Store.GetProfile";
    $http.get(url, {params: {'storeId': $rootScope.user.storeId, 'userId': $rootScope.user.id}})
        .success(function (data, status, headers, config) {
            $scope.storeProfile = data;
            if (!$scope.storeProfile.printers) {
                $scope.storeProfile.printers = [];
            }
            if (!$scope.storeProfile.staffEmails) {
                $scope.storeProfile.staffEmails = [];
            }
            if (!$scope.storeProfile.staffEmails) {
                $scope.storeProfile.staffEmails = [];
            }
            if (!$scope.storeProfile.gateway) {
                $scope.storeProfile.gateway = {};
            }
            $scope.storeProfile.gateway.cedIp = $window.localStorage.getItem("cedIP");
        })
        .error(function (data, status, headers, config) {
            $scope.popupMsg = data;
            ngDialog.open({scope: $scope});
        }
    );
}

function updateStoreProfile($scope, $rootScope, $http) {
    console.log("Profile", $scope.storeProfile);
    var url = getURL($rootScope) + "/flow/POS.Store.UpdateProfile?storeId=" + $rootScope.user.storeId;
    $http.post(url, $scope.storeProfile)
        .success(function (data, status, headers, config) {
            $scope.message = "Saved";
            $rootScope.storeProfile = $scope.storeProfile;
        })
        .error(function (data, status, headers, config) {
            $scope.message = "Error: " + data + ". Please try again";
        }
    );
}
posControllers.controller('SetupStoreCtrl', function ($window,$controller, $cookieStore, $rootScope, $scope, $http, ngDialog) {
    $scope.fonts = ["Courier",
        "Courier-Bold",
        "Courier-BoldOblique",
        "Courier-Oblique",
        "Courier New",
        "CourierNewPS-BoldItalicMT",
        "CourierNewPS-BoldMT",
        "CourierNewPS-ItalicMT",
        "CourierNewPSMT",
        "Menlo-Bold",
        "Menlo-BoldItalic",
        "Menlo-Italic",
        "Menlo-Regular"];

    $scope.timeZones= ["EST",
        "CST",
        "MST",
        "PST"];

    getStoreProfile($window,$http, $rootScope, $scope, ngDialog);

    $scope.updateStoreProfile = function () {
        updateStoreProfile($scope, $rootScope, $http);
    }
});

posControllers.controller('SetupStaffCtrl', function ($window,$controller, $cookieStore, $rootScope, $scope, $http, ngDialog) {
    getStoreProfile($window,$http, $rootScope, $scope, ngDialog);

    $scope.removeStaffEmail = function (staffEmail) {
        var index = $scope.storeProfile.staffEmails.indexOf(staffEmail);
        if (index != -1) {
            $scope.storeProfile.staffEmails.splice(index, 1);
        }
    };

    $scope.addStaffEmail = function () {
        if ($scope.storeProfile.newStaffEmail) {
            var index = $scope.storeProfile.staffEmails.indexOf($scope.storeProfile.newStaffEmail);
            if (index == -1) {
                $scope.storeProfile.staffEmails.push($scope.storeProfile.newStaffEmail);
            }
            $scope.storeProfile.newStaffEmail = "";
        }
    };

    $scope.updateStoreProfile = function () {
        updateStoreProfile($scope, $rootScope, $http);
    };

    $scope.removeCloseOutStaffEmail = function (staffEmail) {
        var index = $scope.storeProfile.closeOutStaffEmails.indexOf(staffEmail);
        if (index != -1) {
            $scope.storeProfile.closeOutStaffEmails.splice(index, 1);
        }
    };

    $scope.addCloseOutStaffEmail = function () {
        if ($scope.storeProfile.newCloseOutStaffEmail) {
            var index = $scope.storeProfile.closeOutStaffEmails.indexOf($scope.storeProfile.newCloseOutStaffEmail);
            if (index == -1) {
                $scope.storeProfile.closeOutStaffEmails.push($scope.storeProfile.newCloseOutStaffEmail);
            }
            $scope.storeProfile.newCloseOutStaffEmail = "";
        }
    };

});

posControllers.controller('SetupGatewayCtrl', function ($window, $controller, $cookieStore, $rootScope, $scope, $http, ngDialog) {
    getStoreProfile($window,$http, $rootScope, $scope, ngDialog);
    //$scope.storeProfile.gateway.cedIp = $window.localStorage.get("cedIP");

    $scope.checkDeviceStatus = function () {
        if ($scope.storeProfile.gateway.cedIp.length > 0) {
            callExternalURL($scope.storeProfile.gateway.cedIp + "?Action=Status&Format=JSON", function (data) {
                if (data) {
                    var json = JSON.parse(data);
                    var status = json.Status;
                    var serial = json.SerialNumber;
                    var appVersion = json.ApplicationVersion;

                    var msg = "Status: " + status + ", Version: " + appVersion + ", Serial #: " + serial;
                    $scope.popupMsg = msg;
                    ngDialog.open({scope: $scope});
                }
            });
        }
    };

    $scope.updateStoreProfile = function () {
        updateStoreProfile($scope, $rootScope, $http);
        $window.localStorage.setItem("cedIP",$scope.storeProfile.gateway.cedIp);
    };
});

posControllers.controller('SetupPrinterCtrl', function ($window,$controller, $cookieStore, $rootScope, $scope, $http, ngDialog) {
    var storage = window.localStorage;
    $scope.config = new Object();

    $scope.config.cardReaderIP = storage.getItem("cardReaderIP");
    $scope.config.printerIP = storage.getItem("printerIP");
    $scope.config.labelPrinterIP = storage.getItem("labelPrinterIP");

    getStoreProfile($window,$http, $rootScope, $scope, ngDialog);

    $scope.searchPrinters = function () {
        $scope.popupMsg = "Searching receipt printers ...";
        var dlg = ngDialog.open({template: 'templates/simple-no-button-dialog.html', scope: $scope});
        setTimeout(function () {
            $scope.loadPrinters(dlg);
        }, 200);
    };

    $scope.searchLabelPrinters = function () {
        $scope.popupMsg = "Searching label printers ...";
        var dlg = ngDialog.open({template: 'templates/simple-no-button-dialog.html', scope: $scope});
        setTimeout(function () {
            $scope.loadLabelPrinters(dlg);
        }, 200);
    };

    $scope.loadPrinters = function (dlg) {
        if (window.cordova !== undefined) {
            cordova.exec(
                function callback(data) {
                    console.log("First printer ip " + data[0]);
                    console.log('Succeeded ');
                    $scope.config.printers = data;
                    $scope.$apply();
                    dlg.close();
                },
                // Register the errorHandler
                function errorHandler(err) {
                    alert("Can't search printers");
                },
                'StarPrinter',
                'searchPrinters',
                []
            );
        }
    };

    $scope.openCashRegister = function () {
        openCashRegister();
    };

    $scope.testReceiptPrint = function () {
        if (window.cordova !== undefined) {
            var sessionKey = $cookieStore.get('sessionKey');
            var printerIP = window.localStorage.getItem("printerIP");
            var contents = '\n\n\n\ Test Receipt \n\n\n';
            var user = $rootScope.user;
            contents += ' Store : ' + user.storeName + '\n';
            contents += ' User : ' + user.firstName + ' ' + user.lastName + '\n';
            contents += ' Date : ' + new Date() + '\n';
            contents += ' Printer IP : ' + printerIP + '\n\n\n';

            cordova.exec(
                function callback(data) {
                    console.log('Succeeded : ' + data);
                },
                // Register the errorHandler
                function errorHandler(err) {
                    alert("Error while printing : " + err);
                },
                'StarPrinter',
                'printLabel',
                [sessionKey, printerIP, contents]
            );
        }
    };

    $scope.loadLabelPrinters = function (dlg) {
        if (window.cordova !== undefined) {
            cordova.exec(
                function callback(data) {
                    console.log("First label printer ip " + data[0]);
                    console.log('Succeeded ');
                    $scope.config.labelPrinters = data;
                    $scope.$apply();
                    dlg.close();
                },
                // Register the errorHandler
                function errorHandler(err) {
                    alert("Can't search label printers");
                },
                'ZebraPrinter',
                'searchPrinters',
                []
            );
        }
    };

    $scope.setPrinter = function () {
        if ($scope.config.selectedPrinter) {
            $scope.config.printerIP = $scope.config.selectedPrinter;
        }
        var storage = window.localStorage;
        window.localStorage.setItem("printerIP", $scope.config.printerIP);
    };

    $scope.setLabelPrinter = function () {
        if ($scope.config.selectedLabelPrinter) {
            $scope.config.labelPrinterIP = $scope.config.selectedLabelPrinter;
        }
        var storage = window.localStorage;
        window.localStorage.setItem("labelPrinterIP", $scope.config.labelPrinterIP);
    };

    $scope.testLabelPrint = function () {
        if (window.cordova !== undefined) {
            var printerIP = window.localStorage.getItem("labelPrinterIP");
            var testLabel = '^XA^LRN^MNY^MFN,N^LH10,12^MCY^POI^PW812^CI27^FO620,1140^FT20,630^CVY^BD2^FH_^FD003840325780000[)>_1E01_1D961Z95010216_1DUPSN_1D6E6585_1E07L7HZ7E/YO+25:K38L7.B7F#ENA.$L/CZ\"\" Z#$-%H(J_0D_1E_04^FS^FT15,23^A0N,20,24^FVDEPOSCO^FS^FT15,42^A0N,20,24^FV770-123-4567^FS^FT15,61^A0N,20,24^FV7135 10TH STREET E^FS^FT15,81^A0N,20,24^FVSARASOTA  FL 34243^FS^FT60,183^A0N,28,32^FVAWESOME PEEPS^FS^FT60,213^A0N,28,32^FV203-921-9971^FS^FT60,244^A0N,28,32^FV1111 PRINCIPAL DR^FS^FT60,288^A0N,45,44^FVALPHARETTA  GA 30007^FS^FT380,30^A0N,30,34^FV1 LBS ^FS^FT673,34^A0N,28,32^FV 1 OF 1^FS^FT620,736^A0N,100,76^FV   ^FS^FO677,640^GB123,123,122^FS^FT300,618^BY3^BCN,103,N,N,,A^FV42032578^FS^FT290,493^A0N,80,70^FVFL 325 1-22^FS^FT10,704^A0N,56,58^FVUPS GROUND^FS^FT10,737^A0N,26,30^FVTRACKING #: 1Z 6E6 585 03 9501 0216^FS^FO0,762^GB800,4,4^FS^FT790,1039^A0N,22,26^FV ^FS^FT10,1035^A0N,22,26^FVBILLING: ^FS^FT126,1035^A0N,22,26^FVP/P ^FS^FT10,1151^A0N,22,26^FVTrx Ref No.: 12345678^FS^FT10,1173^A0N,22,26^FVPart No.: 24S_0413^FS^FT15,153^A0N,28,32^FVSHIP TO: ^FS^FO0,637^GB798,14,14^FS^FO0,997^GB800,14,14^FS^FO0,416^GB800,4,4^FS^FO240,416^GB3,221,3^FS^FT190,1188^A0N,14,20^FVXOL 13.07.17          NV45 36.0A 01/2013^FS^FT105,982^BY3^BCN,202,N,N,,A^FV1Z6E65850395010216^FS^XZ^XZ';
            cordova.exec(
                function callback(data) {
                    console.log('Succeeded : ' + data);
                },
                // Register the errorHandler
                function errorHandler(err) {
                    alert("Error while printing : " + err);
                },
                'ZebraPrinter',
                'printLabel',
                ['', '', '123', printerIP, '9100', 'zpl', testLabel]
            );
        }
    };

    $scope.addPrinter = function () {
        if ($scope.storeProfile.newPrinter) {
            var index = $scope.storeProfile.printers.indexOf($scope.storeProfile.newPrinter);
            if (index == -1) {
                $scope.storeProfile.printers.push($scope.storeProfile.newPrinter);
            }
            $scope.storeProfile.newPrinter = "";
        }
    };

});

posControllers.controller('SetupAssociatesCtrl', function ($window,$controller, $cookieStore, $rootScope, $scope, $http, ngDialog) {
    getStoreProfile($window,$http, $rootScope, $scope, ngDialog);

    $http.get(getURL($rootScope) + "/flow/POS.Store.GetAssociates", {params: {'storeId': $rootScope.user.storeId, 'userId': $rootScope.user.id}})
        .success(function (data, status, headers, config) {
            if (data) {
                $scope.storeAssociates = data;
            }
            else {
                $scope.storeAssociates = [];
            }
        })
        .error(function (data, status, headers, config) {
            $scope.message = "Error while loading Sales Associates";
        }
    );

    $scope.addNewSalesAssociate = function () {
        if ($scope.storeAssociates.newAssociate) {
            var index = $scope.storeAssociates.indexOf($scope.storeAssociates.newAssociate);
            if (index == -1) {
                $http.post(getURL($rootScope) + "/flow/POS.Store.AddNewAssociate?storeId=" + $rootScope.user.storeId + "&companyId=" + $rootScope.user.companyId, $scope.storeAssociates.newAssociate)
                    .success(function (data, status, headers, config) {
                        $scope.message = "Added";

                        console.log(data);

                        $scope.storeAssociates.push(data);
                        $scope.storeAssociates.newAssociate = {};
                    })
                    .error(function (data, status, headers, config) {
                        $scope.message = "Error: Please try again";
                        $scope.storeAssociates.newAssociate = {};
                    }
                );
            }
        }
    };

    $scope.removeSalesAssociate = function (associate) {
        var index = $scope.storeAssociates.indexOf(associate);
        if (index != -1) {
            $http.delete(getURL($rootScope) + "/flow/POS.Store.DeleteSalesAssociate?userId=" + associate.id)
                .success(function (data, status, headers, config) {
                    $scope.message = "Removed";

                    $scope.storeAssociates.splice(index, 1);
                })
                .error(function (data, status, headers, config) {
                    $scope.message = "Error: " + data + ". Please try again";
                }
            );
        }
    };

    $scope.updateStoreAssociates = function () {
        var url = getURL($rootScope) + "/flow/POS.Store.UpdateSalesAssociates?storeId=" + $rootScope.user.storeId;
        $http.post(url, $scope.storeAssociates)
            .success(function (data, status, headers, config) {
                $scope.message = "Saved";
            })
            .error(function (data, status, headers, config) {
                $scope.message = "Error: " + data + ". Please try again";
            }
        );
    };

    $scope.updateStoreAssociate = function (associate, index) {
        var url;

        if (index > -1) {
            url = getURL($rootScope) + "/flow/POS.Store.UpdateSalesAssociate?storeId=" + $rootScope.user.storeId;
        }
        else {
            url = getURL($rootScope) + "/flow/POS.Store.AddNewAssociate?storeId=" + $rootScope.user.storeId + "&companyId=" + $rootScope.user.companyId;
        }
        $http.post(url, associate)
            .success(function (data, status, headers, config) {
                associate.id = data.id;

                if (index > -1) {
                    $scope.storeAssociates[index] = associate;
                }
                else {
                    $scope.storeAssociates.push(associate);
                }
                $scope.message = "Saved";
            })
            .error(function (data, status, headers, config) {
                $scope.message = "Error: " + data + ". Please try again";
            }
        );
    };

    $scope.openAssociate = function (associate) {
        var index = $scope.storeAssociates.indexOf(associate);
        if (associate == null) {
            $scope.selectedAssociate = {"id": 0, "firstName": '', "lastName": '', "username": '', "password": '', "pin": '', "maxDiscount": '', "margin": '', "salesGoal": ''};
        }
        else {
            $scope.selectedAssociate = {
                "id": associate.id,
                "firstName": associate.firstName,
                "lastName": associate.lastName,
                "username": associate.username,
                "password": associate.password,
                "pin": associate.pin,
                "maxDiscount": associate.maxDiscount,
                "margin": associate.margin,
                "salesGoal": associate.salesGoal
            };
        }

        var associateDialog = ngDialog.open({template: 'templates/edit-associate-dialog.html', scope: $scope, controller: 'SetupEditAssociateCtrl'});

        associateDialog.closePromise.then(function (data) {
            var returnValue = data.value;

            if (returnValue == 1) {
                $scope.updateStoreAssociate($scope.selectedAssociate, index);
            }
        });
    };
});

posControllers.controller('SetupEditAssociateCtrl', function ($controller, $scope) {
    $scope.closeDialog = function () {
        $scope.errorMsg = "";

        $scope.closeThisDialog(0);
    };

    $scope.saveAssociate = function () {
        var associate = $scope.selectedAssociate;

        if (!associate.firstName || associate.firstName.length < 1) {
            $scope.errorMsg = "Please enter First Name";
            return;
        }
        if (!associate.lastName || associate.lastName.length < 1) {
            $scope.errorMsg = "Please enter Last Name";
            return;
        }
        if (!associate.username || associate.username.length < 1) {
            $scope.errorMsg = "Please enter Username";
            return;
        }
        if (associate.id < 1) {
            if (!associate.password || associate.password.length < 1) {
                $scope.errorMsg = "Please enter password";
                return;
            }
        }
        if (!associate.pin || associate.pin.length < 1) {
            $scope.errorMsg = "Please enter PIN Code";
            return;
        }
        if (!associate.maxDiscount || associate.maxDiscount.length < 1) {
            $scope.errorMsg = "Please enter MAX Discount";
            return;
        }
        if (!associate.margin || associate.margin.length < 1) {
            $scope.errorMsg = "Please enter Margin";
            return;
        }
        if (!associate.salesGoal || associate.salesGoal.length < 1) {
            $scope.errorMsg = "Please enter Sales Goal";
            return;
        }

        $scope.errorMsg = "";
        $scope.closeThisDialog(1);
    };
});

posControllers.controller('SetupIvuLotoCtrl', function ($window,$controller, $scope, $rootScope, $http, ngDialog) {
    getStoreProfile($window,$http, $rootScope, $scope, ngDialog);

    $scope.updateStoreProfile = function () {
        updateStoreProfile($scope, $rootScope, $http);
    }
});